<?php
session_start();
include 'conexion.php';
$conexion = new retorna_url();
$url = $conexion->retornaURL();

if ($_SESSION['hunter_usuario_nivel'] != "1" && $_SESSION['hunter_usuario_nivel'] != "2"){
    $_SESSION['mensaje_servidor'] = "Token Incorrecto";
    $_SESSION['url_servidor'] = "index.html";
    echo '<meta http-equiv="Refresh" content="0.1;url='.$url.'">';
} else {
    $dispositivo_id = $_POST['dispositivo_id'];
    $producto_id = $_POST['producto_id'];
    
    echo '<table style="background-color: lightgray" class="table table-striped table-hover" id="datatable" cellspacing="0" width="100%">
                                        <thead>
                                          <tr>
                                            <th>Nombre</th>
                                            <th>Descripci&oacute;n</th>
                                            <th>Valor ($)</th>
                                            <th>Cantidad</th>
                                            <th>Medida</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                          </tr>
                                        </thead>
                                        <tbody>';
    
    $consulta = mysql_query("SELECT * FROM dispositivo_producto WHERE dispositivo_id = '$dispositivo_id' AND dispositivo_producto_estado > -1");
    while ($dato = mysql_fetch_array($consulta)){
        echo "<tr>";
        if ($producto_id != $dato['producto_id']){
            $consulta_producto = mysql_query("SELECT * FROM producto WHERE producto_id = '".$dato['producto_id']."'");
            if ($dato_producto = mysql_fetch_array($consulta_producto)){
                echo "<th>".$dato_producto['producto_nombre']."</th>";
                echo "<th>".$dato_producto['producto_descripcion']."</th>";
                echo "<th>".number_format($dato_producto['producto_valor'], 2, '.', ',')."</th>";
                echo "<th>".$dato['dispositivo_producto_cant']."</th>";
                echo "<th>".$dato_producto['producto_medida']."</th>";
            } else {
                echo "<th></th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "<th></th>";
            }
            
            if ($dato['dispositivo_producto_estado'] == "1"){
                echo "<th>Activo</th>";
            } else {
                echo "<th>Inactivo</th>";
            }
            echo '<th>
                  <div class="btn-group text-right">
                      <button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Opciones
                        <span class="caret ml5"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="#" data-effect="mfp-flipInY" onClick="eliminarProducto(\''.$dato['producto_id'].'\', \''.$dispositivo_id.'\')" >Eliminar</a>
                        </li>
                        <li>';
                        if ($dato['dispositivo_producto_estado'] == "1")
                          echo '<a href="#" onClick="cambiarEstadoProducto(\''.$dato['producto_id'].'\',\'0\', \''.$dispositivo_id.'\')">Inactivar</a>';
                        else
                            echo '<a href="#" onClick="cambiarEstadoProducto(\''.$dato['producto_id'].'\',\'1\', \''.$dispositivo_id.'\')">Activar</a>
                        </li>';
                        echo '
                      </ul>
                    </div>
                </th>';
        } else if ($producto_id == $dato['producto_id'] && $_POST['eliminar'] == "1") {
            $consulta_producto = mysql_query("SELECT * FROM producto WHERE producto_id = '".$dato['producto_id']."'");
            if ($dato_producto = mysql_fetch_array($consulta_producto)){
                echo "<th>".$dato_producto['producto_nombre']."</th>";
                echo "<th>".$dato_producto['producto_descripcion']."</th>";
                echo "<th>".number_format($dato_producto['producto_valor'], 2, '.', ',')."</th>";
                echo "<th>".$dato['dispositivo_producto_cant']."</th>";
                echo "<th>".$dato_producto['producto_medida']."</th>";
            } else {
                echo "<th></th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "<th></th>";
            }
            if ($dato['dispositivo_producto_estado'] == "1"){
                echo "<th>Activo</th>";
            } else {
                echo "<th>Inactivo</th>";
            }
            echo '<th>
                  <input type="button" value="Eliminar" class="btn btn-danger" onClick="confirmaEliminarProducto(\''.$dato['producto_id'].'\', \''.$dispositivo_id.'\')"/>
                  </br>
                  <input type="button" value="Cancelar" class="btn btn-info" onClick="productosDispositivo(\''.$dispositivo_id.'\')"/>
                </th>';
        }           
        echo "</tr>";
    }
    
    echo '</tbody></table></br>';
}
?>
