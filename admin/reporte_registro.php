<?php
session_start();
include 'conexion.php';
$conexion = new retorna_url();
$url = $conexion->retornaURL();

if ($_SESSION['hunter_usuario_nivel'] != "7" && $_SESSION['hunter_usuario_nivel'] != "1" && 
    $_SESSION['hunter_usuario_nivel'] != "2" && 
    $_SESSION['hunter_usuario_nivel'] != "3" && 
    $_SESSION['hunter_usuario_nivel'] != "4"){
    $_SESSION['mensaje_servidor'] = "Token Incorrecto";
    $_SESSION['url_servidor'] = "index.html";
    echo '<meta http-equiv="Refresh" content="0.1;url='.$url.'">';
} else {
    
    $reporte_observacion = $_POST['txtObservacion'];
    $dispositivo_id = $_POST['slDispositivo'];
    
    date_default_timezone_set('America/Bogota');
    $horaSistema = date('H:i:s');
    $fechaSistema = date('d/m/Y');


    $query = mysql_query("SELECT * FROM reporte WHERE reporte_estado > 0 AND dispositivo_id = '$dispositivo_id'");
    if (mysql_num_rows($query) > 0){
        echo "El dispositivo ya tiene un reporte activo";
    } else {
        //REGISTRO EN BD
        if (mysql_query("INSERT INTO reporte 
            (cliente_id,
            dispositivo_id,
            reporte_observacion,
            pais_id,
            ciudad_id,
            reporte_fecha,
            reporte_hora,
            reporte_estado) 
        VALUES ('".$_SESSION['hunter_usuario_id']."', 
            '$dispositivo_id',
            '$reporte_observacion',
            '".$_SESSION['hunter_usuario_pais']."',
            '".$_SESSION['hunter_usuario_ciudad']."',
            '$fechaSistema', 
            '$horaSistema', 
            '1')")){

            $consulta_dispositivo = mysql_query("SELECT * FROM dispositivo WHERE dispositivo_id = '$dispositivo_id'");
            if ($dato_dispositivo = mysql_fetch_array($consulta_dispositivo)){
                $cliente_latitud = $dato_dispositivo['dispositivo_latitud'];
                $cliente_longitud = $dato_dispositivo['dispositivo_longitud'];
                $pi = 3.14159;
                $latitud_cliente_radianes = $cliente_latitud * ($pi / 180);
                $longitud_cliente_radianes = $cliente_longitud * ($pi / 180);
            }

            $consulta_reporte = mysql_query("SELECT * FROM reporte WHERE reporte_estado > 0 AND cliente_id = '".$_SESSION['hunter_usuario_id']."' AND 
                    dispositivo_id = '$dispositivo_id' ORDER BY reporte_id DESC");
            if ($dato_reporte = mysql_fetch_array($consulta_reporte)){

                //CALCULAR DISTANCIA DE LOS CAIS MAS CERCANOS
                if (strlen($_SESSION['hunter_usuario_pais']) > 0 && strlen($_SESSION['hunter_usuario_ciudad']) > 0){
                    $consulta_cai = mysql_query("SELECT * FROM cai WHERE cai_estado > 0 AND pais_id = '".$_SESSION['hunter_usuario_pais']."' AND ciudad_id = '".$_SESSION['hunter_usuario_ciudad']."'");
                } else {
                    $consulta_cai = mysql_query("SELECT * FROM cai WHERE cai_estado > 0");
                }
                
                while ($dato_cai = mysql_fetch_array($consulta_cai)) {
                    //COMPARAR DISTANCIAS ENTRE EL CAI Y EL DISPOSITIVO
                    $latitud_cai_radianes = $dato_cai['cai_latitud'] * ($pi / 180);
                    $longitud_cai_radianes = $dato_cai['cai_longitud'] * ($pi / 180);

                    $cambio_latitud = ($cliente_latitud - $dato_cai['cai_latitud']) * ($pi / 180);
                    $cambio_longitud = ($cliente_longitud - $dato_cai['cai_longitud']) * ($pi / 180);

                    $a = sin($cambio_latitud / 2) * sin($cambio_latitud / 2) + cos($latitud_cai_radianes) * 
                            cos($latitud_cliente_radianes) * 
                        sin($cambio_longitud / 2) * sin($cambio_longitud / 2);

                    $c = 2 * atan2(sqrt($a) , sqrt(1 - $a));

                    $distancia = 6371000 * $c;
                    $distancia /= 1000;

                    if ($distancia < 1000){
                        mysql_query("INSERT INTO reporte_detalle (reporte_id, 
                            cai_id, 
                            reporte_detalle_observacion,
                            reporte_detalle_fecha, 
                            reporte_detalle_hora, 
                            reporte_detalle_estado) VALUES ('".$dato_reporte['reporte_id']."',
                            '".$dato_cai['cai_id']."', 
                            '',
                            '$fechaSistema',
                            '$horaSistema',
                            '1')");

                        mysql_query("UPDATE reporte SET pais_id = '".$dato_cai['pais_id']."',
                                        ciudad_id = '".$dato_cai['ciudad_id']."' WHERE reporte_id = '".$dato_reporte['reporte_id']."'");
                    }
                }
            }

            echo "El reporte se ha registrado correctamente";
        } else {
            echo "Ocurrio un problema al registrar el reporte";
        }
    }
}

?>
