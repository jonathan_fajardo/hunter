$(document).ready(function(){
 
    $(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function()
    {
        //obtenemos un array con los datos del archivo
        var file = $("#archivo_img")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo
        showMessage("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
    });
 
    //al enviar el formulario
    $('#btnGuardarItem').click(function(){
        
        if ($("#txtNombreItem").get(0).value != "" && 
            $("#txtDescripcionItem").get(0).value != "" && 
            $("#txtCostoItem").get(0).value != "" && 
            $("#txtPrecioItem").get(0).value != "" && 
            $("#archivo_img").get(0).value != null){
        
            //información del formulario
            var formData = new FormData();
            formData.append("item_id", $("#item_id").get(0).value);
            formData.append("txtNombreItem", $("#txtNombreItem").get(0).value);
            formData.append("txtDescripcionItem", $("#txtDescripcionItem").get(0).value);
            formData.append("txtPrecioItem", $("#txtPrecioItem").get(0).value);
            formData.append("txtCostoItem", $("#txtCostoItem").get(0).value);
            formData.append("archivo_img", archivo_img.files[0]);

            //var formData = new FormData($(".formulario")[0]);

            var url = "";
            if ($("#tipo_peticion").get(0).value == "1")
                url = "item_registro.php";
            else
                url = "item_editar.php";

            var message = ""; 
            //hacemos la petición ajax  
            $.ajax({
                url: url,  
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function(){
                    message = $("<span class='before'>Subiendo la imagen, por favor espere...</span>");
                    showMessage(message);    
                },
                //una vez finalizado correctamente
                success: function(data){
                    message = $("<span class='success'>"+data+"</span>");
                    showMessage(message);
                    if(isImage(fileExtension)) {
                        //$(".showImage").html("<img src='"+data+"' />");
                        $("#txtNombreItem").get(0).value = ""; 
                        $("#txtDescripcionItem").get(0).value = ""; 
                        $("#txtPrecioItem").get(0).value = ""; 
                        $("#txtCostoItem").get(0).value = ""; 
                        $("#img_view").attr("src","holder.js/200x200");
                        $("#formulario").submit();
                    }
                    
                    if (data == "El item se ha modificado correctamente"){
                        $("#formulario").submit();
                    }
                    
                },
                //si ha ocurrido un error
                error: function(){
                    message = $("<span class='error'>Ha ocurrido un error.</span>");
                    showMessage(message);
                }
            });
        } else {
            message = $("<span class='error'>Por favor verifique los campos</span>");
            showMessage(message);
        }
           
    });
});
 
//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}
 
//comprobamos si el archivo a subir es una imagen
//para visualizarla una vez haya subido
function isImage(extension)
{
    switch(extension.toLowerCase()) 
    {
        case 'jpg': case 'gif': case 'png': case 'jpeg':
            return true;
        break;
        default:
            return false;
        break;
    }
}