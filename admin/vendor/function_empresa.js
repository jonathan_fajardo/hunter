$(document).ready(function(){
 
    $(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function()
    {
        //obtenemos un array con los datos del archivo
        var file = $("#archivo_img")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo
        showMessage("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
    });
 
    //al enviar el formulario
    $('#btnGuardarEmpresa').click(function(){
                
        if ($("#txtNombreEmpresa").get(0).value != "" && 
            $("#txtDescripcionEmpresa").get(0).value != "" && 
            $("#txtDescripcionInglesEmpresa").get(0).value != "" && 
            $("#usuario_id").get(0).value != "" && 
            $("#txtDireccionEmpresa").get(0).value != "" && 
            $("#txtDireccionInglesEmpresa").get(0).value != "" && 
            $("#txtTelefonoEmpresa").get(0).value != "" && 
            $("#txtEmailEmpresa").get(0).value != "" && 
            $("#slCiudad").get(0).value != "0" && 
            $("#archivo_img").get(0).value != null){
        
            
        
            //información del formulario
            var formData = new FormData();
            formData.append("empresa_id", $("#empresa_id").get(0).value);
            formData.append("usuario_id", $("#usuario_id").get(0).value);
            formData.append("txtNombreEmpresa", $("#txtNombreEmpresa").get(0).value);
            formData.append("txtDescripcionEmpresa", $("#txtDescripcionEmpresa").get(0).value);
            formData.append("txtDescripcionInglesEmpresa", $("#txtDescripcionInglesEmpresa").get(0).value);
            formData.append("txtDireccionEmpresa", $("#txtDireccionEmpresa").get(0).value);
            formData.append("txtDireccionInglesEmpresa", $("#txtDireccionInglesEmpresa").get(0).value);
            formData.append("txtTelefonoEmpresa", $("#txtTelefonoEmpresa").get(0).value);
            formData.append("txtEmailEmpresa", $("#txtEmailEmpresa").get(0).value);
            
            formData.append("txtLiveSecretStripe", $("#txtLiveSecretStripe").get(0).value);
            formData.append("txtLivePublishableStripe", $("#txtLivePublishableStripe").get(0).value);
            
            formData.append("txtClientIdPayPal", $("#txtClientIdPayPal").get(0).value);
            formData.append("txtSecretPayPal", $("#txtSecretPayPal").get(0).value);

            formData.append("txtLatitud", $("#txtLatitud").get(0).value);
            formData.append("txtLongitud", $("#txtLongitud").get(0).value);
            
            formData.append("slCiudad", $("#slCiudad").get(0).value);
            formData.append("archivo_img", archivo_img.files[0]);

            //var formData = new FormData($(".formulario")[0]);
            
            

            var url = "";
            if ($("#tipo_peticion").get(0).value == "1")
                url = "empresa_registro.php";
            else
                url = "empresa_editar.php";

            var message = ""; 
            //hacemos la petición ajax  
            $.ajax({
                url: url,  
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function(){
                    message = $("<span class='before'>Subiendo la imagen, por favor espere...</span>");
                    showMessage(message);    
                },
                //una vez finalizado correctamente
                success: function(data){
                    message = $("<span class='success'>"+data+"</span>");
                    showMessage(message);
                    if(isImage(fileExtension)) {
                        //$(".showImage").html("<img src='"+data+"' />");
                        //$("#formulario").submit();
                    }
                    
                    if (data == "La empresa se ha modificado correctamente" || data == "La empresa se ha registrado correctamente"){
                        $("#txtNombreEmpresa").get(0).value = ""; 
                        $("#txtDescripcionEmpresa").get(0).value = ""; 
                        $("#txtDescripcionInglesEmpresa").get(0).value = ""; 
                        $("#txtEmailEmpresa").get(0).value = ""; 
                        $("#txtTelefonoEmpresa").get(0).value = ""; 
                        $("#txtDireccionEmpresa").get(0).value = ""; 
                        $("#txtDireccionInglesEmpresa").get(0).value = ""; 
                        
                        $("#txtLiveSecretStripe").get(0).value = ""; 
                        $("#txtLivePublishableStripe").get(0).value = ""; 
                        
                        $("#txtClientIdPayPal").get(0).value = ""; 
                        $("#txtSecretPayPal").get(0).value = ""; 

                        $("#txtLatitud").get(0).value = ""; 
                        $("#txtLongitud").get(0).value = ""; 
                        
                        $("#slCiudad").get(0).value = "0"; 
                        $("#img_view").attr("src","assets/img/placeholder.png");
                        $("#formulario").submit();
                    }
                    
                },
                //si ha ocurrido un error
                error: function(){
                    message = $("<span class='error'>Ha ocurrido un error.</span>");
                    showMessage(message);
                }
            });
        } else {
            message = $("<span class='error'>Por favor verifique los campos</span>");
            showMessage(message);
        }
           
    });
});
 
//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}
 
//comprobamos si el archivo a subir es una imagen
//para visualizarla una vez haya subido
function isImage(extension)
{
    switch(extension.toLowerCase()) 
    {
        case 'jpg': case 'gif': case 'png': case 'jpeg':
            return true;
        break;
        default:
            return false;
        break;
    }
}