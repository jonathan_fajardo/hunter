$(document).ready(function(){
 
    $(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function()
    {
        //obtenemos un array con los datos del archivo
        var file = $("#archivo_img")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo
        showMessage("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
    });
 
    //al enviar el formulario
    $('#btnGuardarCiudad').click(function(){
        
        if ($("#txtNombreCiudad").get(0).value != ""){
        
            //información del formulario
            var formData = new FormData();
            formData.append("pais_id", $("#pais_id").get(0).value);
            formData.append("ciudad_id", $("#ciudad_id").get(0).value);
            formData.append("txtNombreCiudad", $("#txtNombreCiudad").get(0).value);

            //var formData = new FormData($(".formulario")[0]);

            var url = "";
            if ($("#tipo_peticion").get(0).value == "1")
                url = "pais_ciudad_registro.php";
            else
                url = "pais_ciudad_editar.php";

            //alert($("#ciudad_id").get(0).value);

            var message = ""; 
            //hacemos la petición ajax  
            $.ajax({
                url: url,  
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function(){
                    message = $("<span class='before'>Registrando ciudad, por favor espere...</span>");
                    showMessage(message);    
                },
                //una vez finalizado correctamente
                success: function(data){
                    message = $("<span class='success'>"+data+"</span>");
                    showMessage(message);
                    if(isImage(fileExtension)) {
                        //$(".showImage").html("<img src='"+data+"' />");
                        $("#txtNombreCiudad").get(0).value = ""; 
                        //$("#formulario").submit();
                        ciudadesPais($("#pais_id").get(0).value);
                    }
                    
                    if (data == "La ciudad se ha modificado correctamente" || data == "La ciudad se ha registrado correctamente"){
                        //$("#formulario").submit();
                        ciudadesPais($("#pais_id").get(0).value);
                    }
                    
                },
                //si ha ocurrido un error
                error: function(){
                    message = $("<span class='error'>Ha ocurrido un error.</span>");
                    showMessage(message);
                }
            });
        } else {
            message = $("<span class='error'>Por favor verifique los campos</span>");
            showMessage(message);
        }
           
    });
});
 
//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}
 
//comprobamos si el archivo a subir es una imagen
//para visualizarla una vez haya subido
function isImage(extension)
{
    switch(extension.toLowerCase()) 
    {
        case 'jpg': case 'gif': case 'png': case 'jpeg':
            return true;
        break;
        default:
            return false;
        break;
    }
}