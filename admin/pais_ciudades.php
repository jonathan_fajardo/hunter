<?php
session_start();
include 'conexion.php';
$conexion = new retorna_url();
$url = $conexion->retornaURL();

if ($_SESSION['hunter_usuario_nivel'] != "1"){
    $_SESSION['mensaje_servidor'] = "Token Incorrecto";
    $_SESSION['url_servidor'] = "index.html";
    echo '<meta http-equiv="Refresh" content="0.1;url='.$url.'">';
} else {
    $pais_id = $_POST['pais_id'];
    $ciudad_id = $_POST['ciudad_id'];
    
    echo '<table style="background-color: lightgray" class="table table-striped table-hover" id="datatable" cellspacing="0" width="100%">
                                        <thead>
                                          <tr>
                                            <th>Nombre</th>
                                            <th>Estado</th>
                                            <th>Opciones</th>
                                          </tr>
                                        </thead>
                                        <tbody>';
    
    $consulta = mysql_query("SELECT * FROM ciudad WHERE pais_id = '$pais_id' AND ciudad_estado > -1");
    while ($dato = mysql_fetch_array($consulta)){
        echo "<tr>";
        if ($ciudad_id != $dato['ciudad_id']){
            echo "<th>".$dato['ciudad_nombre']."</th>";
            if ($dato['ciudad_estado'] == "1"){
                echo "<th>Activo</th>";
            } else {
                echo "<th>Inactivo</th>";
            }
            echo '<th>
                  <div class="btn-group text-right">
                      <button type="button" class="btn btn-success br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Opciones
                        <span class="caret ml5"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="#" data-effect="mfp-flipInY" onClick="editarCiudad(\''.$dato['ciudad_id'].'\', \''.$pais_id.'\', \''.$dato['ciudad_nombre'].'\')" >Editar</a>
                        </li>
                        <li>
                          <a href="#" data-effect="mfp-flipInY" onClick="eliminarCiudad(\''.$dato['ciudad_id'].'\', \''.$pais_id.'\')" >Eliminar</a>
                        </li>
                        <li>';
                        if ($dato['ciudad_estado'] == "1")
                          echo '<a href="#" onClick="cambiarEstadoCiudad(\''.$dato['ciudad_id'].'\',\'0\', \''.$pais_id.'\')">Inactivar</a>';
                        else
                            echo '<a href="#" onClick="cambiarEstadoCiudad(\''.$dato['ciudad_id'].'\',\'1\', \''.$pais_id.'\')">Activar</a>
                        </li>';
                        echo '
                      </ul>
                    </div>
                </th>';
        } else if ($ciudad_id == $dato['ciudad_id'] && $_POST['eliminar'] == "1") {
            echo "<th>".$dato['ciudad_nombre']."</th>";
            if ($dato['ciudad_estado'] == "1"){
                echo "<th>Activo</th>";
            } else {
                echo "<th>Inactivo</th>";
            }
            echo '<th>
                  <input type="button" value="Eliminar" class="btn btn-danger" onClick="confirmaEliminarCiudad(\''.$dato['ciudad_id'].'\', \''.$pais_id.'\')"/>
                  </br>
                  <input type="button" value="Cancelar" class="btn btn-info" onClick="ciudadesPais(\''.$pais_id.'\')"/>
                </th>';
        }           
        echo "</tr>";
    }
    
    echo '</tbody></table></br>';
}
?>
