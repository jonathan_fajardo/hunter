<?php

$dispositivo = "eN-kpbmGYlo:APA91bElfZAYVaVj2cmUh6FhYyxNuYRbAFlaTVt24cUANDAQttLqUGdkp07ycUWc7wjMp_NfMErGuSEVRHwQF6FLvWE-wqI9-3KMSYG376VefwfihcJd6qR_BcUyAM9SXWvBXgcgYFKC";

$headers = array('Content-Type:application/json', 'Authorization:key=AAAAo7m_Xvg:APA91bFexhIVbna11QfjL-gEqQT3TCsTujB4jWDRC_tLLgQhGGb7IUhT6Pv-mB6mCDSnirDk8iyTAabRC1IubU-JoaXaeK8XdbDvX7zaLyMYnlwwHHtitiM6LOfUnh8rdAMoteo2jLIdXN9o7yTHRFF-Uf31dZlTEg');

$data_message = array("message"=>"HOLA");
$data = array(
  'to'=>$dispositivo,
  "data"=>$data_message);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
if ($headers)
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

echo json_encode($data);

$response = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
if (curl_errno($ch)) {
    echo 'fail';
}
if ($httpCode != 200) {
    echo 'status code '.$httpCode.' '.$response;
}
curl_close($ch);

echo $response;
?>