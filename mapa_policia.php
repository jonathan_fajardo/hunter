<!DOCTYPE html>
<html>
<?php
$dispositivo_latitud = 2.936419;
$dispositivo_longitud = -75.288697;
?>
<head>
	<title>Find My Bicy - Rodar nunca fue tan seguro</title>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />-->
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

	<!-- Datatables CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/datatables/media/css/dataTables.bootstrap.css">

  

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
  
  
  <!-- FullCalendar Plugin CSS -->
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/magnific/magnific-popup.css">

  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/tagmanager/tagmanager.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/daterange/daterangepicker.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/colorpicker/css/bootstrap-colorpicker.min.css">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Theme CSS -->
  	<link rel="stylesheet" type="text/css" href="admin/assets/skin/default_skin/css/theme.css">

  	<!-- Admin Forms CSS -->
  	<link rel="stylesheet" type="text/css" href="admin/assets/admin-tools/admin-forms/css/admin-forms.min.css">

  	<!-- Favicon -->
  <link rel="shortcut icon" href="admin/assets/img/favicon.ico">

	<style type="text/css">

		#btn-contacto-login:hover,
		#btn-contacto-login:focus{
			background:black;
			color: #53c5ea;
		}

		#btn-contacto-login{
			color: #FFF;
		}

		#map {
			height: 80%!important;
	        left: 0px;
	        right: 0px;
	        top: 0px;
	        bottom: 0px;


	    }
	    /* Optional: Makes the sample page fill the window. */
	    html, body {
	        height: 100%;
	        margin: 0;
	        padding: 0;
	        overflow-x:hidden; 
			overflow-y:hidden;

	    }

	    .boton-file {
	    	background-image: "images/icon_file.png";

	    }

	</style>

</head>
<body>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


	<!--navbar navbar-default -->
	<nav class="navbar  navbar-static-top" style="background: black; height: 40px;">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header" style="background: black;">
	      
	      <a class="navbar-brand" href="index.php" style="margin-top: -15px; margin-left: 20px;">
	      		<img src="images/logo_img.png" style="height: 40px; margin-top: 5px; margin-right: 20px; margin-bottom: 5px;" id="img_logo_nav" />
	      </a>
	    </div>




	    <div class="dropdown navbar-right" style="margin-right: 25px; margin-top: 10px; background: black;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="images/icon_hamburger.png" style="width: 30px;" /></a>
          <ul class="dropdown-menu" style="background: black; color: white;">
          	<li><a id="btn-contacto-login" href="perfil_policia.php">Perfil</a></li>
          	<li><a id="btn-contacto-login" href="">Reportes</a></li>
            <li><a id="btn-contacto-login" href="contacto.php">Contacto</a></li>

            <!--<li role="separator" class="divider"></li>-->
          </ul>
        </div>


	  </div><!-- /.container-fluid -->
	</nav>








	



	<div id="map"></div>



	<div class="row" style="position: absolute; left: 30px; top: 200px; text-align: center; width: 200px; height: 400px; background: white;">
		<input type="text" placeholder="Buscar por cedula" class="form-control" />
		<table>
			<tr>
				<td style="color: black; padding: 10px;">Resultado 1</td>
			</tr>
			<tr>
				<td style="color: black; padding: 10px;">Resultado 2</td>
			</tr>
			<tr>
				<td style="color: black; padding: 10px;">Resultado 3</td>
			</tr>
			<tr>
				<td style="color: black; padding: 10px;">Resultado 4</td>
			</tr>
			<tr>
				<td style="color: black; padding: 10px;">Resultado 5</td>
			</tr>
			<tr>
				<td style="color: black; padding: 10px;">Resultado 6</td>
			</tr>
			<tr>
				<td style="color: black; padding: 10px;">Resultado 7</td>
			</tr>
			<tr>
				<td style="color: black; padding: 10px;">Resultado 8</td>
			</tr>
			<tr>
				<td style="color: black; padding: 10px;">Resultado 9</td>
			</tr>
		</table>
	</div>





	<div class="modal fade" id="modoparqueo" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:0px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <!--<div class="modal-header" style="background: white; border-radius: 10px">
	                <button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	                <!--<h4 class="modal-title" id="loginLabel">Ingresa</h4>- ->
	                <!--<img src="images/logo_login.png" style="width: 135px;" />- ->
	            </div>-->
	            <!--<button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>-->
	            <div class="modal-body">
					<button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea; color: #FFF;"><b>VIBRACIÓN</b></button>
					<button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #FFF; color: #000;">ZONA</button>
					<button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #FFF; color: #000;">MOVIMIENTO</button>
					<button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #FFF; color: #000;">OFF</button>

	            </div>
	            <!--<div class="modal-footer" style="text-align: center;">
	                
	            </div>-->
	        </div><!-- /modal content -->
	    </div><!-- /modal dialog -->
	</div><!-- /modal holder -->


	








	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>

	

	<script type="text/javascript">
	jQuery(document).ready(function() {


		$("#txt_numero_tarjeta").attr("maxlength", 16);
		$("#txt_cvc_tarjeta").attr("maxlength", 4);

		$("#img_view_bicy").attr("src","admin/assets/img/placeholder.png");
		$("#img_view_factura").attr("src","admin/assets/img/placeholder.png");
		$("#img_view_tarjeta").attr("src","admin/assets/img/placeholder.png");




		$("#img_logo_nav").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_over_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  }
		});

		$("#img_logo_nav_text").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_over_img.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_img.png');
		  }
		});


		




		$(window).on({
		 "scroll" : function() {
		    //$("#div-nav").height(50);
		    $("div-nav").css("height", 50);
		  }
		});

	});
	</script>


	<script>
      var customLabel = {
        restaurant: {
          label: 'R'
        },
        bar: {
          label: 'B'
        }
      };

        function initMap() {

            var dispositivo_latitud = <?php echo $dispositivo_latitud; ?>;
            var dispositivo_longitud = <?php echo $dispositivo_longitud; ?>;
            //var dispositivo_nombre = <?php echo $dispositivo_nombre; ?>;
            var dispositivo_nombre = decodeURIComponent("<?php echo rawurlencode($dispositivo_nombre); ?>");

            var map = new google.maps.Map(document.getElementById('map'), {
              center: new google.maps.LatLng(dispositivo_latitud, dispositivo_longitud),
              zoom: 16
            });
            
            var infoWindow = new google.maps.InfoWindow;

            //var name = dispositivo_nombre;
            var address = "";
            var type = "";
            var point = new google.maps.LatLng(
                parseFloat(dispositivo_latitud),
                parseFloat(dispositivo_longitud));

            var infowincontent = document.createElement('div');
            var strong = document.createElement('strong');
            strong.textContent = dispositivo_nombre
            infowincontent.appendChild(strong);
            infowincontent.appendChild(document.createElement('br'));

            var text = document.createElement('text');
            text.textContent = address
            infowincontent.appendChild(text);

            var icon = customLabel[type] || {};
            var marker = new google.maps.Marker({
              map: map,
              position: point,
              label: icon.label
            });
            marker.addListener('click', function() {
              infoWindow.setContent(infowincontent);
              infoWindow.open(map, marker);
            });
        }

        initMap();


    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSASLjBcHC5YEIPjqSo8Rs2KZoF3REG3c&callback=initMap">
    </script>



    <!-- jQuery -->
  <script src="admin/vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="admin/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- HighCharts Plugin -->
  <script src="admin/vendor/plugins/highcharts/highcharts.js"></script>

  <!-- JvectorMap Plugin + US Map (more maps in plugin/assets folder) -->
  <script src="admin/vendor/plugins/jvectormap/jquery.jvectormap.min.js"></script>
  <script src="admin/vendor/plugins/jvectormap/assets/jquery-jvectormap-us-lcc-en.js"></script>

  <!-- FullCalendar Plugin + moment Dependency -->
  <script src="admin/vendor/plugins/fullcalendar/lib/moment.min.js"></script>
  <script src="admin/vendor/plugins/fullcalendar/fullcalendar.min.js"></script>
  
  <!-- Magnific Popup Plugin -->
  <script src="admin/vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <!--<script src="admin/assets/js/utility/utility.js"></script>
  <script src="admin/assets/js/demo/demo.js"></script>
  <script src="admin/assets/js/main.js"></script>-->

  <script src="admin/assets/js/demo/demo.js"></script>
  <script src="admin/assets/js/main.js"></script>
  
  <!-- Select2 Plugin Plugin -->
  <script src="admin/vendor/plugins/select2/select2.min.js"></script>
  
  
  
  
  
  
  <!-- Time/Date Plugin Dependencies -->
  <script src="admin/vendor/plugins/globalize/globalize.min.js"></script>
  <script src="admin/vendor/plugins/moment/moment.min.js"></script>
  
  <!-- Bootstrap Maxlength plugin -->
  <script src="admin/vendor/plugins/maxlength/bootstrap-maxlength.min.js"></script>
  
  <!-- DateTime Plugin -->
  <script src="admin/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>
  
  <!-- MaskedInput Plugin -->
  <script src="admin/vendor/plugins/jquerymask/jquery.maskedinput.min.js"></script>
  
  
  
  
  <!-- FileUpload JS -->
  <script src="admin/vendor/plugins/fileupload/fileupload.js"></script>
  <script src="admin/vendor/plugins/holder/holder.min.js"></script>

  <!-- Tagmanager JS -->
  <script src="admin/vendor/plugins/tagsinput/tagsinput.min.js"></script>
  
  
  
  
  
  
  
  
  <!-- Datatables -->
  <script src="admin/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>
  
  <!-- Datatables Tabletools addon -->
  <script src="admin/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

  <!-- Datatables ColReorder addon -->
  <script src="admin/vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>

  <!-- Datatables Bootstrap Modifications  -->
  <script src="admin/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>


  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    //Core.init();

    // Init Demo JS  
    Demo.init();

    // select list dropdowns - placeholder like creation
    var selectList = $('.admin-form select');
    selectList.each(function(i, e) {
      $(e).on('change', function() {
        if ($(e).val() == "0") $(e).addClass("empty");
        else $(e).removeClass("empty")
      });
    });
    selectList.each(function(i, e) {
      $(e).change();
    });

    // Init TagsInput plugin
    $("input#tagsinput").tagsinput({
      tagClass: function(item) {
        return 'label bg-primary light';
      }
    });

  });
  </script>




  <style type="text/css">
       
        .file-upload {
            position: relative;
            overflow: hidden;
            margin: 10px; 
        }

        .file-upload input.file-input {
          position: absolute;
          top: 0;
          right: 0;
          margin: 0;
          padding: 0;
          font-size: 20px;
          cursor: pointer;
          opacity: 0;
          filter: alpha(opacity=0); 
        }


        
        
        
    </style>

</body>
</html>	