<!DOCTYPE html>
<html lang="en">
<header>
    <div class="container clearfix">
        <h1 id="logo">
            LOGO
        </h1>
        <nav>
            <a href="">Lorem</a>
            <a href="">Ipsum</a>
            <a href="">Dolor</a>
        </nav>
    </div>
    
</header><!-- /header -->
<body>

	<div id="royal_preloader"></div>



	<!-- Begin Boxed or Fullwidth Layout -->
	<div id="bg-boxed">
	    <div class="boxed">

			


			<div id="carrusel" class="hidden-xs" style="height: 600px; width: 100%;">
			  	<div id="item1-carrusel">
		      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 600px; width: 100%;">
		    	</div>
			    <div id="item2-carrusel" style="height: 600px; width: 100%; display: none; overflow: hidden;">
					<video loop="loop" style="width: 100%;" autoplay="autoplay">
				        <source src="videos/hunter.mp4" type="video/mp4"/>
				    </video>
			    </div>

			    <div class="carousel-caption" style="color: white; background: transparent; margin-top: -200px; font-size: 30px; font-family: Verdana; text-align: center;">
	        		<p style="color: white;  width: 100%; "><b>Rodar</b> nunca fue tan <b>seguro</b></p>
	      		</div>
	      		<div class="carousel-caption" style="color: white; background: transparent; margin-top: -100px; font-size: 15px; font-family: Verdana;">
	        		<p style="color: white; width: 100%;">Para <strong>todas las bicicletas</strong></p>
	      		</div>
	      		<div class="carousel-caption" style="color: white; background: transparent; margin-top: 0px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
	        		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 18px; font-family: 'Arial Black', Gadget, sans-serif; font-weight: bold;">Find My Bicy</a>
	      		</div>
			</div>


			<div id="carrusel-small" class="hidden-md hidden-lg hidden-sm" style="height: 300px; width: 100%;">
			  	<div id="item1-carrusel-small">
		      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 300px; width: 100%;">
		    	</div>
			    <div id="item2-carrusel-small" style="height: 300px; width: 100%; display: none; overflow: hidden;">
					<video loop="loop" style="width: 100%;" autoplay="autoplay">
				        <source src="videos/hunter.mp4" type="video/mp4"/>
				    </video>
			    </div>

			    <div class="carousel-caption" style="color: white; background: transparent; margin-top: -400px; font-size: 20px; font-family: Verdana; text-align: center;">
	        		<p style="color: white;  width: 100%; "><b>Rodar</b> nunca fue tan <b>seguro</b></p>
	      		</div>
	      		<div class="carousel-caption" style="color: white; background: transparent; margin-top: -300px; font-size: 12px; font-family: Verdana;">
	        		<p style="color: white; width: 100%;">Para <strong>todas las bicicletas</strong></p>
	      		</div>
	      		<div class="carousel-caption" style="color: white; background: transparent; margin-top: -200px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
	        		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: 'Arial Black', Gadget, sans-serif; font-weight: bold;">Find My Bicy</a>
	      		</div>
			</div>






			<!--<div>
				<div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 600px; width: 100%">
				  <!-- Indicators - ->
				  <!--<ol class="carousel-indicators">
				    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#myCarousel" data-slide-to="1"></li>
				  </ol>- ->

				  <!-- Wrapper for slides - ->
				  <div class="carousel-inner" role="listbox">
				    
				    <div class="item active" style="height: 600px; width: 100%">
				      <img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 600px">

				      <div class="carousel-caption" style="color: white; background: transparent; margin-bottom: 300px">
				        <span style="color: white; font-size: 40px; width: 100%">Rodar nunca fue tan seguro</span><b />
				        <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
				      </div>

				    </div>

				    <div class="item" >
				    	<div style="text-align: center; justify-content: center; align-content: center; flex-direction: column;">
				    		<video width="100%" height="100%" autoplay loop>
							  	<source src="videos/hunter.mp4" type="video/mp4">
								Your browser does not support the video tag.
							</video>
				    	</div>
				    </div>

				    

				  </div>

				  <!-- Left and right controls -->
				  <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>- ->
				</div>
			</div>-->









			<!-- Start Slider Revolution -->
			<!--<div class="rev_slider_wrapper">
				<div class="rev_slider" data-version="5.0" id="slider1">
					<ul>

						<!-- Slide 1 - ->
						<li data-delay="10000"
						data-description="Raleway Bootstrap Template"
						data-transition="zoomin"
						data-easein="default"
						data-easeout="default"
						data-masterspeed="default"
						data-param1="test"
						data-rotate="0"
						data-thumb="revolution/assets/images/hunter.jpg"
						data-slotamount="default"
						data-title="Find My Bicy">

							<div class="rs-background-video-layer"
							data-forcerewind="on"
							data-volume="mute"
							data-forcerewind="on"
							data-volume="mute"
							data-videowidth="100%"
							data-videoheight="100%"
							data-videomp4="videos/hunter.mp4"
							data-videopreload="preload"
							data-videoloop="loopandnoslidestop"
							data-forceCover="1"
							data-aspectratio="16:9"
							data-autoplay="true"
							data-autoplayonlyfirsttime="false"
							data-nextslideatend="true"
							></div>

							<div class="tp-caption NotGeneric-Title tp-resizeme rs-parallaxlevel-0"
							data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
							data-fontsize="['30','30','30','25']"
							data-lineheight="['70','70','70','50']"
							data-whitespace="nowrap"
							data-transform_idle="o:1;"
							data-transform_in="z:0;rX:0;rY:0;rZ:5deg;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Back.easeOut;"
							data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="1000"
							style="z-index: 6; white-space: nowrap;">Rodar nunca fue tan seguro!
							</div>

							<div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0"
							data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
							data-transform_idle="o:1;"
							data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
							data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
							data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="1500"
							style="z-index: 7; white-space: nowrap;">Para <strong>todas las bicicletas</strong>
							</div>

							<div class="tp-caption"
							data-x="500"
							data-y="105"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="y:10px;opacity:0;s:700;e:Power3.easeOut;"
							data-transform_out="y:10px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:15px;font-family:'Raleway' sans-serif;font-weight:200;  white-space: nowrap;">
							Para <strong>todas las bicicletas</strong>.
							</div>

							<!--<div class="tp-caption NotGeneric-Icon tp-resizeme rs-parallaxlevel-0"
							data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
							data-transform_idle="o:1;"
							data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;"
							data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
							data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="2000"
							style="z-index: 8;font-size:50px;line-height:52px; white-space: nowrap;"><i class="fa fa-bicycle"></i>
							</div>-->

						</li>

						<!-- Slide 2 - ->
						<li data-delay="10000"
						data-description="Some Description"
						data-easein="default"
						data-easeout="default"
						data-masterspeed="default"
						data-param1="test"
						data-rotate="0"
						data-slotamount="default"
						data-thumb="images/backgrounds/hunter.jpg"
						data-title="Descripción"
						data-transition="slideremoveleft">

							<!-- MAIN IMAGE - ->
							<img alt=""
							data-bgfit="cover"
							data-bgposition="center center"
							data-bgrepeat="no-repeat"
							data-no-retina=""
							height="800"
							src="images/backgrounds/hunter.jpg"
							style="background-color:#000;"
							width="1732">

							<div class="tp-caption"
							data-x="500"
							data-y="105"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="y:10px;opacity:0;s:700;e:Power3.easeOut;"
							data-transform_out="y:10px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:35px;font-family:'Raleway' sans-serif;font-weight:200;border:2px solid #fff;padding:0 17px;">
							Tu bicicleta segura, en solo <strong>3 pasos</strong>.
							</div>

							<div class="tp-caption background-mainsub"
							data-x="500"
							data-y="195"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:100px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:100px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:27px;font-family:'FontAwesome' sans-serif;line-height:50px;text-align:center;padding-left:12px;padding-right:12px;border-radius:50%">

								<span class="fa fa-check"></span>

							</div>

							<div class="tp-caption"
							data-x="560"
							data-y="200"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:130px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:130px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:24px;font-family:'Open Sans' sans-serif;font-weight:300;">
							Crea tu cuenta de usuario.
							</div>

							<div class="tp-caption background-mainsub"
							data-x="500"
							data-y="260"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:160px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:160px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:27px;font-family:'FontAwesome' sans-serif;line-height:50px;text-align:center;padding-left:12px;padding-right:12px;border-radius:50%">

								<span class="fa fa-check"></span>

							</div>

							<div class="tp-caption"
							data-x="560"
							data-y="265"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:190px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:190px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:24px;font-family:'Open Sans' sans-serif;font-weight:300;">
							Adquiere tu dispositivo <strong>Hunter</strong> en nuestra tienda o en puntos autorizados.
							</div>

							<div class="tp-caption background-mainsub"
							data-x="500"
							data-y="324"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:220px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:220px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:27px;font-family:'FontAwesome' sans-serif;line-height:50px;text-align:center;padding-left:12px;padding-right:12px;border-radius:50%">

								<span class="fa fa-check"></span>

							</div>

							<div class="tp-caption"
							data-x="560"
							data-y="329"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:260px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:260px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:24px;font-family:'Open Sans' sans-serif;font-weight:300;">
							Descarga nuestra aplicaci&oacute;n.
							</div>

						</li>

					</ul>
				</div>
			</div>--><!-- /rev slider -->
			<!-- End Slider Revolution -->

			<!-- Begin Menu -->
			<div class="pt40 pb40 border-bottom">
				<div class="container">
					<br>
					<p class="lead text-center">Menu <b><mark>Find My Bicy</mark></b></p>
					<hr style="width:400px">
					<div class="row">
						<!-- Content 1 -->
						<div class="col-sm-4 fadeInUp-animated text-center">
						    <a href="registro.php"><span class="ion-person bordered-icon-lg bordered-icon-color"></span></a>
						    <h4 class="mt15">Reg&iacute;strate.</h4>
						</div>
						<!-- Content 2 -->
						<div class="col-sm-4 fadeInUp-animated text-center mt20-xs">
						    <a href="tienda.php"><span class="ion-bag bordered-icon-lg bordered-icon-color"></span></a>
						    <h4 class="mt15">Tienda.</h4>
						</div>
						<!-- Content 3 -->
						<!--<div class="col-sm-3 fadeInUp-animated text-center">
						    <a href="servicios.html"><span class="ion-grid bordered-icon-lg bordered-icon-color"></span></a>
						    <h4 class="mt15">Servicios.</h4>
						</div>-->
						<!-- Content 4 -->
						<div class="col-sm-4 fadeInUp-animated text-center mt20-xs">
						    <a href="contacto.php"><span class="ion-email bordered-icon-lg bordered-icon-color"></span></a>
						    <h4 class="mt15">Contacto.</h4>
						</div>
					</div>
				</div><!-- /container -->
			</div><!-- /cta -->
			<!-- End Menu -->

			<!-- Begin Content Section -->
			<section class="background-light-grey border-top">
				<div class="container">
					<br>
					<div class="row mt40 mb40">

						<!-- Begin Carousel -->
						<div class="col-sm-4">
							<div id="aboutCarousel" class="carousel carousel-fade slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class="carousel-indicators">
									<li data-target="#aboutCarousel" data-slide-to="0" class="active"></li>
									<li data-target="#aboutCarousel" data-slide-to="1"></li>
									<li data-target="#aboutCarousel" data-slide-to="2"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<div style="background-image:url('images/backgrounds/stock1.jpg'); height:255px;" data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"></div>
									</div>
									<div class="item">
										<div style="background-image:url('images/backgrounds/stock2.jpg'); height:255px" data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"></div>
									</div>
									<div class="item">
										<div style="background-image:url('images/backgrounds/stock3.jpg'); height:255px" data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"></div>
									</div>
								</div>

								<!-- Controls -->
								<a class="left carousel-control" href="#aboutCarousel" data-slide="prev">
									<span class="ion-ios7-arrow-left carousel-arrow-left no-lineheight"></span>
								</a>
								<a class="right carousel-control" href="#aboutCarousel" data-slide="next">
									<span class="ion-ios7-arrow-right carousel-arrow-right no-lineheight"></span>
								</a>
							</div><!-- /carousel -->
						</div><!-- /column -->
						<!-- End Carousel -->

						<!-- Content 1 -->
						<div class="col-sm-8 mt30-xs">
							<div class="heading"><h4>Find My Bicy</h4></div>
							<p>Find My Bicy surge con la necesidad de promover el uso seguro de la bicicleta. El servicio se ofrece mediante un dispositivo gps que muestra a nuestros clientes la ubicación de su bicicleta en cualquier momento, presta servicios de parqueo, con alta cobertura a nivel nacional, y mediante alianza con Policía Nacional y Fiscalía General de la Nación.</p>
		
						</div><!-- /column -->
						<!-- End Content 1 -->
					</div><!-- /row-->
				</div><!-- /container -->
			</section><!-- /section -->
			<!-- End Content Section -->

			<!-- Begin Footer -->
			<footer class="footer">
				<div class="container">
					<div class="row">

						<!-- Social -->
						<div class="col-sm-6 mg25-xs">
							<div class="heading-footer"><h4>Redes Sociales</h4></div>
								<span href="shop-single-product.html" class="fa fa-facebook bordered-icon-dark bordered-icon-sm mb5"></span>
								<span class="fa fa-twitter bordered-icon-dark bordered-icon-sm mb5 mt10"></span>
								<span class="fa fa-youtube bordered-icon-dark bordered-icon-sm mb5"></span>
						</div>

						<!-- Contact -->
						<div class="col-sm-6 mg25-xs">
							<div class="heading-footer"><h4>Cont&aacute;ctanos</h4></div>
							<p><span class="ion-home footer-info-icons"></span><small class="address">Calle 00 # 00-00 Bogot&aacute;</small></p>
							<p><span class="ion-email footer-info-icons"></span><small class="address"><a href="mailto:info@findmybicy.com">info@findmybicy.com</a></small></p>
							<p><span class="ion-ios7-telephone footer-info-icons"></span><small class="address">+573156607087</small></p>
						</div>

					</div><!-- /row -->

					<!-- Copyright -->
					<div class="row">
						<hr class="dark-hr">
						<div class="col-sm-11 col-xs-10">
							<p class="copyright">© 2017 Find My Bicy. Todos los derechos reservados.</b></p>
						</div>
						<div class="col-sm-1 col-xs-2 text-right">
							<a href="#" class="scroll-top"><div class="footer-scrolltop-holder"><span class="ion-ios7-arrow-up footer-scrolltop"></span></div></a>
						</div>
					</div><!-- /row -->
				</div><!-- /container -->
			</footer><!-- /footer -->
			<!-- End Footer -->

		</div><!-- /boxed -->
	</div><!-- /bg boxed-->
	<!-- End Boxed or Fullwidth Layout -->

	<input type="hidden" id="ocultoCarrusel" value="0" />

	<!-- Javascript Files -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="js/scrollReveal.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>
	<script type="text/javascript" src="js/style-switcher.js"></script><!-- Remove for production -->
	<script type="text/javascript" src="js/activate-snippet.js"></script>
	<script type="text/javascript" src="js/skrollr.min.js"></script>

	<!-- On Scroll Animations - scrollReveal.js -->
    <script>
		var config = {
		easing: 'hustle',
		mobile:  true,
		delay:  'onload'
		}
		window.sr = new scrollReveal( config );
    </script>

	<!-- Slider Revolution JS -->
	<script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>

	<!-- Slider Revolution Extensions (Load Extensions only on Local File Systems The following part can be removed on Server for On Demand Loading) -->
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<!-- Slider Revolution Main -->
	<script type="text/javascript">
	jQuery(document).ready(function() {


		function explode(){
			if ($("#ocultoCarrusel").get(0).value == "0"){
				$("#ocultoCarrusel").get(0).value = "1";
				$("#item1-carrusel").fadeOut(300, function(){
					$("#item2-carrusel").fadeIn(300);
				});
				$("#item1-carrusel-small").fadeOut(300, function(){
					$("#item2-carrusel-small").fadeIn(300);
				});
			} else {
				$("#ocultoCarrusel").get(0).value = "0";
				$("#item2-carrusel").fadeOut(300, function(){
					$("#item1-carrusel").fadeIn(300);
				});
				$("#item2-carrusel-small").fadeOut(300, function(){
					$("#item1-carrusel-small").fadeIn(300);
				});
			}
		  setTimeout(explode, 10000);
		}
		setTimeout(explode, 10000);







		$("#img_logo_nav").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_over_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  }
		});

		$("#img_logo_nav_text").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_over_img.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_img.png');
		  }
		});


		$("#img_loggin").on({
		 "mouseover" : function() {
		    this.src = 'images/icon_user_jj.png';
		  },
		  "mouseout" : function() {
		    this.src='images/icon_user.png';
		  }
		});




		$(window).on({
		 "scroll" : function() {
		    //$("#div-nav").height(50);
		    $("div-nav").css("height", 50);
		  }
		});

		/*$(window).on("scroll", function() {
		    //$("nav").toggleClass("shrink", $(this).scrollTop() > 50)
		    $("#div-nav").height(50);
		});*/

		


	   jQuery("#slider1").revolution({
	        sliderType:"standard",
	        startDelay:2500,
	        spinner:"spinner2",
	        sliderLayout:"auto",
	        viewPort:{
	           enable:false,
	           outof:'wait',
	           visible_area:'100%'
	        }
	        ,
	        delay:9000,
			navigation: {
				keyboardNavigation:"off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation:"off",
				onHoverStop:"off",
				arrows: {
					style:"erinyen",
					enable:true,
					hide_onmobile:true,
					hide_under:600,
					hide_onleave:true,
					hide_delay:200,
					hide_delay_mobile:1200,
					tmp:'<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div>    <div class="tp-arr-img-over"></div>	<span class="tp-arr-titleholder">{{title}}</span> </div>',
					left: {
						h_align:"left",
						v_align:"center",
						h_offset:30,
						v_offset:0
					},
					right: {
						h_align:"right",
						v_align:"center",
						h_offset:30,
						v_offset:0
					}
				}
				,
				touch:{
					touchenabled:"on",
					swipe_treshold : 75,
					swipe_min_touches : 1,
					drag_block_vertical:false,
					swipe_direction:"horizontal"
				}
				,
				bullets: {
	                enable:true,
	                hide_onmobile:true,
	                hide_under:600,
	                style:"hermes",
	                hide_onleave:true,
	                hide_delay:200,
	                hide_delay_mobile:1200,
	                direction:"horizontal",
	                h_align:"center",
	                v_align:"bottom",
	                h_offset:0,
	                v_offset:30,
	                space:5
				}
			},
			gridwidth:1240,
			gridheight:497
	    });
	});
	</script>



	<style type="text/css">

    header {
	    width: 100%;
	    height: 150px;
	    overflow: hidden;
	    position: fixed;
	    top: 0;
	    left: 0;
	    z-index: 999;
	    background-color: #0683c9;
	    -webkit-transition: height 0.3s;
	    -moz-transition: height 0.3s;
	    -ms-transition: height 0.3s;
	    -o-transition: height 0.3s;
	    transition: height 0.3s;
	}
	header h1#logo {
	    display: inline-block;
	    height: 150px;
	    line-height: 150px;
	    float: left;
	    font-family: "Oswald", sans-serif;
	    font-size: 60px;
	    color: white;
	    font-weight: 400;
	    -webkit-transition: all 0.3s;
	    -moz-transition: all 0.3s;
	    -ms-transition: all 0.3s;
	    -o-transition: all 0.3s;
	    transition: all 0.3s;
	}
	header nav {
	    display: inline-block;
	    float: right;
	}
	header nav a {
	    line-height: 150px;
	    margin-left: 20px;
	    color: #9fdbfc;
	    font-weight: 700;
	    font-size: 18px;
	    -webkit-transition: all 0.3s;
	    -moz-transition: all 0.3s;
	    -ms-transition: all 0.3s;
	    -o-transition: all 0.3s;
	    transition: all 0.3s;
	}
	header nav a:hover {
	    color: white;
	}
	header.smaller {
	    height: 75px;
	}
	header.smaller h1#logo {
	    width: 150px;
	    height: 75px;
	    line-height: 75px;
	    font-size: 30px;
	}
	header.smaller nav a {
	    line-height: 75px;
	}

	@media all and (max-width: 660px) {
	    header h1#logo {
	        display: block;
	        float: none;
	        margin: 0 auto;
	        height: 100px;
	        line-height: 100px;
	        text-align: center;
	    }
	    header nav {
	        display: block;
	        float: none;
	        height: 50px;
	        text-align: center;
	        margin: 0 auto;
	    }
	    header nav a {
	        line-height: 50px;
	        margin: 0 10px;
	    }
	    header.smaller {
	        height: 75px;
	    }
	    header.smaller h1#logo {
	        height: 40px;
	        line-height: 40px;
	        font-size: 30px;
	    }
	    header.smaller nav {
	        height: 35px;
	    }
	    header.smaller nav a {
	        line-height: 35px;
	    }
	}

    </style>
    <script type="text/javascript">
    	function init() {
		    window.addEventListener('scroll', function(e){
		        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
		            shrinkOn = 300,
		            header = document.querySelector("header");
		        if (distanceY > shrinkOn) {
		            classie.add(header,"smaller");
		        } else {
		            if (classie.has(header,"smaller")) {
		                classie.remove(header,"smaller");
		            }
		        }
		    });
		}
		window.onload = init();
    </script>
    

</body>
</html>
