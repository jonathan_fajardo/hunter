<!DOCTYPE html>
<html>
<head>
	<title>Find My Bicy - Rodar nunca fue tan seguro</title>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<style type="text/css">
		#btn-findmybicy:hover,
		#btn-findmybicy:focus{
			background-color:transparent;
		}

		#btn-contacto:hover,
		#btn-contacto:focus{
			background-color:transparent;
		}

		#btn-iniciosesion:hover,
		#btn-iniciosesion:focus{
			background-color:transparent;
		}
	</style>

</head>
<body>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


	<!--navbar navbar-default -->
	<nav class="navbar  navbar-static-top hidden-xs" style="background: black; height: 40px;">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header" style="background: black;">
	      <button type="button" class="navbar-toggle collapsed" style="background-image: url('images/icon_hamburger.png');" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php" style="margin-top: -15px; margin-left: 20px;">
	      		<img src="images/logo_img.png" style="height: 40px; margin-top: 5px; margin-right: 20px; margin-bottom: 5px;" id="img_logo_nav" />
	      </a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="background: black;">
	      <ul class="nav navbar-nav navbar-right" style="background: black; text-decoration: none; height: 40px;">
	        <li style="text-decoration: none;">
		    	<a id="btn-findmybicy" href="findmybicy.php" style="border-style: solid; border-width: 2px; border-color: #53c5ea; color: white; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; font-weight: bold; margin-top: 10px">Find My Bicy</a>
		    </li>
			<li>
		      <a id="btn-contacto" style="color: white; margin-top: 3px;" href="contacto.php">Contacto</a>
		    </li>
		    <li>
		      <a id="btn-iniciosesion" href="" data-toggle="modal" data-target="#login" style="margin-top: 0px; margin-right: 25px;"><img id="img_loggin" src="images/icon_user.png" style="width: 25px;" /></a>
		    </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>



	<!--navbar navbar-default -->
	<nav class="navbar  navbar-static-top hidden-sm hidden-md hidden-lg hidden-xl" style="background: black; height: 40px;">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header" style="background: black;">
	      <button type="button" class="navbar-toggle collapsed" style="background-image: url('images/icon_hamburger.png');" data-toggle="collapse" data-target="#bs-example1-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php" style="margin-top: -15px; margin-left: 20px;">
	      		<img src="images/logo_img.png" style="height: 40px; margin-top: 5px; margin-right: 20px; margin-bottom: 5px;" id="img_logo_nav" />
	      </a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example1-navbar-collapse-1" style="background: black; width: 170px;">
	      <ul class="nav navbar-nav navbar-right" style="background: black; text-decoration: none; height: 140px;">
	        <li style="text-decoration: none;">
		    	<a id="btn-findmybicy" href="findmybicy.php" style="border-style: solid; border-width: 2px; border-color: #53c5ea; color: white; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; font-weight: bold; margin-top: 10px">Find My Bicy</a>
		    </li>
			<li>
		      <a id="btn-contacto" style="color: white; margin-top: 3px;" href="contacto.php">Contacto</a>
		    </li>
		    <li>
		      <a id="btn-iniciosesion" href="" data-toggle="modal" data-target="#login" style="margin-top: 0px; margin-right: 25px;"><img id="img_loggin" src="images/icon_user.png" style="width: 25px;" /></a>
		    </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>





	












	<!-- Login -->
	<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <!--<div class="modal-header" style="background: white; border-radius: 10px">
	                <button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	                <!--<h4 class="modal-title" id="loginLabel">Ingresa</h4>- ->
	                <!--<img src="images/logo_login.png" style="width: 135px;" />- ->
	            </div>-->
	            <!--<button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>-->
	            <div class="modal-body">
								<form role="form">
										<div class="form-group">
												<!--<label>Correo</label>-->
												<div class="input-group" style="width: 100%">
														<!--<div class="input-group-addon"><span class="ion-android-mail" style="font-size:9px;"></span></div>-->
														<input class="form-control" type="email" id="email" placeholder="Ingresa tu correo" style="width: 100%">
												</div>
										</div>
										<div class="form-group">
											<!--<label>Contraseña</label>-->
											<div class="input-group" style="width: 100%">
														<!--<div class="input-group-addon"><span class="ion-ios7-locked"></span></div>-->
													<input type="password" class="form-control" id="password" placeholder="Ingresa tu contraseña">
												</div>
										</div>
										<!--<div class="checkbox">
												<label>
													<input type="checkbox"> Recu&eacute;rdame
												</label>
										</div>-->

										<div class="row" style="padding-left: 20px; padding-right: 20px; padding-top: 5px; padding-bottom: 5px;">
											<span>¿No tienes cuenta? </span><a style="color: #53c5ea;" href="">Reg&iacute;strate</a>
										</div>

										<!--<hr class="mb20 mt15">
										<button type="submit" class="btn btn-rw btn-primary">Submit</button> &nbsp;&nbsp;&nbsp;<small><a href="#">Forgot your password?</a></small>-->
								</form><!-- /form -->
	            </div>
	            <div class="modal-footer" style="text-align: center;">
	                <button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>INGRESAR</b></button>
	            	</br>
	            	</br>
	            	<span>o</span>
	            	</br>
	            	</br>
	            	<a href=""><img src="images/icon_facebook_login.png" style="width: 40px; margin-right: 20px;" /></a>
	            	<a href=""><img src="images/icon_googleplus_login.png" style="width: 40px;" /></a>
	            </div>
	        </div><!-- /modal content -->
	    </div><!-- /modal dialog -->
	</div><!-- /modal holder -->
	<!-- End Login -->








	
	



	<!--<div id="carrusel" class="hidden-xs" style="height: 600px; width: 100%; text-align:center; justify-content: center; align-content: center;">
	  	<div id="item1-carrusel">
      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 600px; width: 100%;">
    	</div>
	    <div id="item2-carrusel" style="height: 600px; width: 100%; display: none; overflow: hidden;">
			<video loop="loop" style="width: 100%;" autoplay="autoplay">
		        <source src="videos/hunter.mp4" type="video/mp4"/>
		    </video>
	    </div>

	    <div style="margin-top: 200px;
    right: 0px;
    left: 0px;
    top: 0px;
    bottom: 0px;
    text-align:center; 
    justify-content: center; 
    align-content: center;
    width: 100%;
    height: 100px;
    padding: 10px;">
	    	<div style="color: white; background: transparent; font-size: 30px; font-family: Verdana; text-align: center;">
	    		<p style="color: white;  width: 100%; "><b>Rodar nunca fue tan seguro</b></p>
	  		</div>
	  		<div style="color: white; background: transparent; font-size: 15px; font-family: Verdana;">
	    		<p style="color: white; width: 100%;">Para <strong>todas las bicicletas</strong></p>
	  		</div>
	  		<div style="color: white; background: transparent; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
	    		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 18px; font-family: Arial, Gadget, sans-serif; " href="#"><b>Find</b> My Bicy &nbsp;<span class="glyphicon glyphicon-chevron-down"></span> </a>
	  		</div>
	    </div>


	    
	</div>-->




	<div id="carrusel" class="row hidden-xs" style="height: 600px; width: 100%; margin-left: 0px; margin-top: -20px;">
	  	<div id="item1-carrusel">
      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 600px; width: 100%;">
    	</div>
	    <div id="item2-carrusel" style="height: 600px; width: 100%; display: none; overflow: hidden;">
			<!--<video loop="loop" style="width: 100%;" autoplay="autoplay">
		        <source src="videos/hunter.mp4" type="video/mp4"/>
		    </video>-->
		    <img src="images/backgrounds/stock3.jpg" alt="Chania" style="height: 600px; width: 100%;">
	    </div>

	    <div style="color: white; background: transparent; margin-top: -350px; font-size: 35px; font-family: Verdana; text-align: center; ">
    		<p style="color: white;  width: 100%; ">Rodar nunca fue tan seguro</p>
  		</div>
  		<div style="color: white; background: transparent; margin-top: 10px; font-size: 15px; font-family: Verdana; text-align: center; margin-bottom: 10px;">
    		<p style="color: white; width: 100%;">Para <b>todas las bicicletas</b></p>
  		</div>
  		<div style="color: white; background: transparent; margin-top: 20px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
    		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 18px; font-family: Arial, Gadget, sans-serif; " href="findmybicy.php"><b>Find</b> My Bicy &nbsp;<span class="glyphicon glyphicon-chevron-down"></span> </a>
  		</div>
	</div>


	









	<div id="carrusel-small" class="hidden-md hidden-lg hidden-sm" style="height: 300px; width: 100%; margin-top: -20px;">
	  	<div id="item1-carrusel-small">
      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 300px; width: 100%;">
    	</div>
	    <div id="item2-carrusel-small" style="height: 300px; width: 100%; display: none; overflow: hidden;">
			<!--<video loop="loop" style="width: 100%;" autoplay="autoplay">
		        <source src="videos/hunter.mp4" type="video/mp4"/>
		    </video>-->
		    <img src="images/backgrounds/stock3.jpg" alt="Chania" style="height: 300px; width: 100%;">
	    </div>

	    <div style="color: white; background: transparent; margin-top: -200px; font-size: 20px; font-family: Verdana; text-align: center;">
    		<p style="color: white;  width: 100%; "><b>Rodar nunca fue tan seguro</b></p>
  		</div>
  		<div style="color: white; background: transparent; margin-top: 0px; font-size: 12px; font-family: Verdana; text-align: center;">
    		<p style="color: white; width: 100%;">Para <b>todas las bicicletas</b></strong></p>
  		</div>
  		<div style="color: white; background: transparent; margin-top: 20px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
    		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; " href="findmybicy.php"><b>Find</b> My Bicy  <span class="glyphicon glyphicon-chevron-down"></span></a>
  		</div>
	</div>


	<!-- Begin Menu -->
	<div class="pt40 pb40 border-bottom">
		<div class="container" style="padding-bottom: 30px; margin-top: 35px;">
			<br>
			<p class="lead text-center" style="font-size: 14px; color: darkgray;">ELLOS CONFIAN EN NOSOTROS</p>
			<hr style="width:400px">
			<div class="row">
				<div class="col-sm-6 col-xs-6 fadeInUp-animated text-center">
					<a href="http://www.ean.edu.co/"><img src="images/logo-ean.png" style="height: 80px;" /></span></a>
				</div>
				<div class="col-sm-6 col-xs-6 fadeInUp-animated text-center">
					<a href="http://www.colcircuitos.com"><img src="http://www.colcircuitos.com/images/hdLogo.png" style="height: 80px;" /></span></a>
				</div>
			</div>
		</div><!-- /container -->
	</div><!-- /cta -->
	<!-- End Menu -->





	<!-- Begin Content Section -->
	<section class="background-light-grey border-top" style="padding-bottom: 80px; padding-top: 60px; background: #EEEEEE;">
		<div class="container">
			<br>
			<div class="row mt40 mb40">

				<!-- Begin Carousel -->
				<div class="col-sm-6">
					<div id="aboutCarousel" class="carousel carousel-fade slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#aboutCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#aboutCarousel" data-slide-to="1"></li>
							<li data-target="#aboutCarousel" data-slide-to="2"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<div class="item active">
								<div style="background-image:url('images/backgrounds/stock1.jpg'); height:255px;" data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"></div>
							</div>
							<div class="item">
								<div style="background-image:url('images/backgrounds/stock2.jpg'); height:255px" data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"></div>
							</div>
							<div class="item">
								<div style="background-image:url('images/backgrounds/stock3.jpg'); height:255px" data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"></div>
							</div>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#aboutCarousel" data-slide="prev">
							<span class="ion-ios7-arrow-left carousel-arrow-left no-lineheight"></span>
						</a>
						<a class="right carousel-control" href="#aboutCarousel" data-slide="next">
							<span class="ion-ios7-arrow-right carousel-arrow-right no-lineheight"></span>
						</a>
					</div><!-- /carousel -->
				</div><!-- /column -->
				<!-- End Carousel -->

				<!-- Content 1 -->
				<div class="col-sm-6 mt30-xs" style="text-align: center;">
					</br>
					</br>
					<div class="heading"><h4>¿Por qu&eacute; Find My Bicy?</h4></div>
					<p>Somos una empresa de origen colombiano legalmente constituida y autorizada, que ofrece seguridad a los BicyUsuarios mediante el uso de herramientas tecnológicas.</p>

				</div><!-- /column -->
				<!-- End Content 1 -->
			</div><!-- /row-->
		</div><!-- /container -->
	</section><!-- /section -->
	<!-- End Content Section -->






	<!-- Begin Content Section -->
	<section class="background-white border-top" style="padding-bottom: 80px; padding-top: 60px;">
		<div class="container">
			<br>
			<div class="row mt40 mb40">

				<!-- Begin Carousel -->
				<div class="col-sm-6">
					
					<h4 style="color: #000">Somos</h4><h1 style="color: #000">&nbsp;Seguridad</h1>
					</br>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilizamos tecnología para ofrecer el nivel más alto de seguridad a los usuarios, mediante nuestra plataforma en página web y aplicativo móvil, donde el usuario podrá acceder a diferentes servicios, en cualquier momento, para mantener georeferenciada y vigilada su bicicleta</p>
					</br>
					</br>
					<div class="row" style="text-align: center;">
						<div class="col-md-4" style="text-align: center; justify-content: center; align-content: center; color: #000;">
							<img style="width:180px;" src="images/icono_servicio_localizacion.png" />
							</br>
							</br>
							<h5><p style=" text-align: center;">Localización bicicleta</p></h5>
						</div>
						<div class="col-md-4" style="text-align: center; color: #000; justify-content: center; align-content: center;">
							<img style="width:180px; " src="images/icono_servicio_parqueo.png" />
							</br>
							</br>
							<h5><p style="text-align: center;">Modo parqueo por vibración o salida de zona</p></h5>
						</div>
						<div class="col-md-4" style="text-align: center; color: #000; justify-content: center; align-content: center;">
							<img style="width:180px; " src="images/icono_servicio_alerta.png" />
							</br>
							</br>
							<h5><p style=" text-align: center;">Alerta a Policía Nacional, Fiscalía General de la Nación, familia y amigos</p></h5>
						</div>
					</div>

				</div><!-- /column -->
				<!-- End Carousel -->

				<!-- Content 1 -->
				<div class="col-sm-6 mt30-xs" style="padding-left: 100px;">
					
					<img src="images/img_bicicletas.jpg" style="width: 120%" />

				</div><!-- /column -->
				<!-- End Content 1 -->
			</div><!-- /row-->
		</div><!-- /container -->
	</section><!-- /section -->
	<!-- End Content Section -->






	<!-- Begin Content Section -->
	<section class="background-light-grey border-top hidden-xs" style="padding-bottom: 80px; padding-top: 80px; background: #EEEEEE;">
		<div class="container">
			<div class="row" style="text-align: center">
				<h1 style="color: #53c5ea">¿Cómo empezar?</h1>
				</br>
				<h5>Disponible para <b>todas las bicicletas</b></h5>
			</div>
			<br>
			<div class="row mt40 mb40">

				<!-- Begin Carousel -->
				<div class="col-sm-4" style="text-align: center;">
					<img src="images/paso1.png" style="height: 200px;" />
					</br>
					</br>
					</br>
					</br>
					<h5><a>Adquirir GPS</a> y pagar servicio.</h5>
					</br>
					</br>
					</br>
					<h6 style="font-size: 11px">El costo periodico se deriva del operador del servicio</h6>

				</div><!-- /column -->
				<!-- End Carousel -->

				<!-- Content 1 -->
				<div class="col-sm-4 mt30-xs" style="text-align: center;">
					<img src="images/paso2.png" style="height: 200px;" />
					</br>
					</br>
					</br>
					</br>
					<h5>Completar registro e instalar GPS en la bicicleta.</h5>
					</br>
					</br>
					<h6 style="font-size: 11px">Ver video instructivo en cuenta de usuario</h6>

				</div><!-- /column -->

				<div class="col-sm-4 mt30-xs" style="text-align: center;">
					<img src="images/paso3.png" style="height: 200px;" />
					</br>
					</br>
					</br>
					</br>
					<h5><a>Accede en plataforma web</a> o descarga nuestro aplicativo móvil.</h5>
					</br>
					</br>
					<div class="row" style="text-align: center">
						<img src="images/logo-appstore.png" style="width: 120px;" />
						<img src="images/logo-googleplay.png" style="width: 120px;" />
					</div>
					
					

				</div><!-- /column -->

				<!-- End Content 1 -->
			</div><!-- /row-->
		</div><!-- /container -->
	</section><!-- /section -->
	<!-- End Content Section -->

	<!-- Begin Content Section -->
	<section class="background-light-grey border-top hidden-sm hidden-md hidden-lg hidden-xl" style="padding-bottom: 80px; padding-top: 80px; background: #EEEEEE;">
		<div class="container">
			<div class="row" style="text-align: center">
				<h1 style="color: #53c5ea">¿Cómo empezar?</h1>
				<h5>Disponible para <b>todas las bicicletas</b></h5>
			</div>
			<br>
			<div class="row mt40 mb40">

				<!-- Begin Carousel -->
				<div class="col-sm-4" style="text-align: center;">
					<img src="images/paso1.png" style="height: 200px;" />
					</br>
					<h5><a>Adquirir GPS</a> y pagar servicio.</h5>
					<h6 style="font-size: 11px">El costo periodico se deriva del operador del servicio</h6>
					</br>
				</div><!-- /column -->
				<!-- End Carousel -->

				<!-- Content 1 -->
				<div class="col-sm-4 mt30-xs" style="text-align: center;">
					<img src="images/paso2.png" style="height: 200px;" />
					</br>
					<h5>Completar registro e instalar GPS en la bicicleta.</h5>
					<h6 style="font-size: 11px">Ver video instructivo en cuenta de usuario</h6>
					</br>
				</div><!-- /column -->

				<div class="col-sm-4 mt30-xs" style="text-align: center;">
					<img src="images/paso3.png" style="height: 200px;" />
					</br>
					<h5><a>Accede en plataforma web</a> o descarga nuestro aplicativo móvil.</h5>
					</br>
					<div class="row" style="text-align: center">
						<img src="images/logo-appstore.png" style="width: 120px;" />
						<img src="images/logo-googleplay.png" style="width: 120px;" />
					</div>
					
					

				</div><!-- /column -->

				<!-- End Content 1 -->
			</div><!-- /row-->
		</div><!-- /container -->
	</section><!-- /section -->
	<!-- End Content Section -->





	<section class="row" style="padding-top: 80px; padding-bottom: 60px;">

		<div class="row" style="text-align: center">
			<h1 style="color: #53c5ea">Testimonios</h1>
			</br>
			<h5>Nuestra prioridad es prestar un servicio de alta seguridad para los BicyUsuarios</h5>
			</br>
			</br>
		</div>
		<div class="row">
			<div class="col-md-6" style="text-align: center;">
				<img src="images/Foto.jpg" style="width: 50%" />
				</br>
				</br>
				</br>
				</br>
			</div>
			<div class="col-md-6" style="padding-top: 100px; padding-left: 30px; padding-right: 150px; padding-bottom: 30px;">
				<p><i>"Gracias a Find My Bicy rodar es más seguro y no he vuelto a perder mi bicicleta. Es un dispositivo tecnológico que ofrece un servicio de alta calidad y cobertura."</i></p>
				</br>
				<h3>Jonathan Fajardo Roa</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6" style="text-align: center;">
				<img src="images/Foto.jpg" style="width: 50%" />
				</br>
				</br>
				</br>
				</br>
			</div>
			<div class="col-md-6" style="padding-top: 100px; padding-left: 30px; padding-right: 150px; padding-bottom: 30px;">
				<p><i>"Find My Bicy mejora la seguridad de los BicyUsuarios en la capital colombiana, incluyéndome. Además es una herramienta que ayuda a la administración municipal, con el control de la delincuencia"</i></p>
				</br>
				<h3>Jonathan Fajardo Roa</h3>
			</div>
		</div>
		<!--<div class="row">
			<div class="col-md-6" style="text-align: center;">
				<img src="images/mariana_pajon.jpg" style="width: 50%" />
				</br>
				</br>
				</br>
				</br>
			</div>
			<div class="col-md-6" style="padding-top: 100px; padding-left: 30px; padding-right: 150px; padding-bottom: 30px;">
				<p><i>"Me gusta Find My Bicy porque me ayuda a mantener vigilada mi bicicleta a toda hora."</i></p>
				</br>
				<h3>Mariana Pajón</h3>
			</div>
		</div>-->
		</br>
		<div class="row" style="text-align: center">
			<h4><a style="color: #53c5ea; text-decoration: none;" href="testimonios.php">Ver Mas</a></h4>
			</br>
		</div>
	</section>









	<div id="carrusel-down" class="row hidden-xs" style="height: 600px; width: 100%; margin-left: 0px">
	  	<div id="item1-carrusel-down">
      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 600px; width: 100%;">
    	</div>
	    <div id="item2-carrusel-down" style="height: 600px; width: 100%; display: none; overflow: hidden;">
			<!--<video loop="loop" style="width: 100%;" autoplay="autoplay">
		        <source src="videos/hunter.mp4" type="video/mp4"/>
		    </video>-->
		    <img src="images/backgrounds/stock3.jpg" alt="Chania" style="height: 600px; width: 100%;">
	    </div>

	    <div style="color: white; background: transparent; margin-top: -350px; font-size: 35px; font-family: Verdana; text-align: center;">
    		<p style="color: white;  width: 100%; ">Vigilemos juntos tu bicicleta</p>
  		</div>
  		<div class="carousel-caption-down" style="color: white; background: transparent; margin-top: 10px; font-size: 15px; font-family: Verdana; text-align: center; margin-bottom: 10px;">
    		<p style="color: white; width: 100%;">Rodar nunca fue tan seguro</p>
  		</div>
  		<div class="carousel-caption-down" style="color: white; background: transparent; margin-top: 20px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
    		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 18px; font-family: Arial, Gadget, sans-serif; " href="findmybicy.php"><b>Find</b> My Bicy &nbsp;<span class="glyphicon glyphicon-chevron-down"></span> </a>
  		</div>
	</div>


	<div id="carrusel-small-down" class="hidden-md hidden-lg hidden-sm" style="height: 300px; width: 100%;">
	  	<div id="item1-carrusel-small-down">
      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 300px; width: 100%;">
    	</div>
	    <div id="item2-carrusel-small-down" style="height: 300px; width: 100%; display: none; overflow: hidden;">
			<!--<video loop="loop" style="width: 100%;" autoplay="autoplay">
		        <source src="videos/hunter.mp4" type="video/mp4"/>
		    </video>-->
		    <img src="images/backgrounds/stock3.jpg" alt="Chania" style="height: 300px; width: 100%;">
	    </div>

	    <div class="carousel-caption-down" style="color: white; background: transparent; margin-top: -400px; font-size: 20px; font-family: Verdana; text-align: center;">
    		<p style="color: white;  width: 100%; ">Vigilemos juntos tu bicicleta</p>
  		</div>
  		<div class="carousel-caption-down" style="color: white; background: transparent; margin-top: -300px; font-size: 12px; font-family: Verdana;">
    		<p style="color: white; width: 100%;">Rodar nunca fue tan seguro</p>
  		</div>
  		<div class="carousel-caption-down" style="color: white; background: transparent; margin-top: -200px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
    		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; " href="findmybicy.php"><b>Find</b> My Bicy &nbsp;<span class="glyphicon glyphicon-chevron-down"></span></a>
  		</div>
	</div>






	<!-- Begin Footer -->
	<footer class="footer" style="padding-top: 80px; background: black;">
		<div class="container">
			<div class="row">

				<!-- Social -->
				<!--<div class="col-sm-4 mg25-xs">
					<!--<div class="heading-footer"><h4>Redes Sociales</h4></div>- ->
						<span href="shop-single-product.html" class="fa fa-facebook bordered-icon-dark bordered-icon-sm mb5"></span>
						<span class="fa fa-instagram bordered-icon-dark bordered-icon-sm mb5 mt10"></span>
						<span class="fa fa-youtube bordered-icon-dark bordered-icon-sm mb5"></span>
						</br>
						</br>
						<div class="row" style="text-align: left">
							<img src="images/logo-appstore.png" style="width: 120px;" />
							<img src="images/logo-googleplay.png" style="width: 120px;" />
						</div>
				</div>-->

				<!-- Contact -->
				<div class="col-sm-6 col-xs-6 mg25-xs" style="padding-left: 40px; font-size: 15px;">
					<!--<div class="heading-footer"><h4 style="color: #53c5ea">Find My Bicy</h4></div>-->
					<p><span style="color: white" class="glyphicon glyphicon-home"></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;¿Quiénes somos?</a></small></p>
					<p><span style="color: white" class="glyphicon glyphicon-bullhorn"></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;TESTIMONIOS</a></small></p>
					<p><span style="color: white" class="glyphicon glyphicon-shopping-cart"></span><small class="address">&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: white;" href="">Puntos de venta</a></small></p>
					<p><span style="color: white" class="glyphicon glyphicon-earphone"></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;Contacto</a></small></p>
					</br>
					<p></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;Términos y condiciones</a></small></p>
					<p></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;Política de privacidad</a></small></p>
					<!--<p><span class="ion-home footer-info-icons"></span><small class="address">Calle 00 # 00-00 Bogot&aacute;</small></p>
					<p><span class="ion-email footer-info-icons"></span><small class="address"><a href="mailto:info@findmybicy.com">info@findmybicy.com</a></small></p>
					<p><span class="ion-ios7-telephone footer-info-icons"></span><small class="address">+573156607087</small></p>-->
				</div>


				<div class="col-sm-6 col-xs-6 mg25-xs">
					<div class="row" style="text-align: center;">
						<a href="" style="margin-right: 10px;"><img src="images/icon_facebook.png" style="width: 40px;" /></a>
						<a href="" style="margin-right: 10px;"><img src="images/icon_instagram.png" style="width: 40px;" /></a>
						<a href="" style="margin-right: 10px;"><img src="images/icon_youtube.png" style="width: 40px;" /></a>
					</div>
					</br>
					</br>
					</br>
					<div style="text-align: center;">
						<a style="border-style: solid; border-width: 2px; border-color: #53c5ea; color: white; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; font-weight: bold; margin-top: 15px" href="findmybicy.php">Find My Bicy</a>
					</div>
					</br>
					<!--<span href="shop-single-product.html" class="fa fa-facebook bordered-icon-dark bordered-icon-sm mb5"></span>
					<span class="fa fa-instagram bordered-icon-dark bordered-icon-sm mb5 mt10"></span>
					<span class="fa fa-youtube bordered-icon-dark bordered-icon-sm mb5"></span>-->
					</br>
					<div class="row" style="text-align: center; margin-left: 2px;">
						<a href=""><img src="images/logo-appstore.png" style="width: 120px; margin-right: 10px;" /></a>
						<a href=""><img src="images/logo-googleplay.png" style="width: 120px;margin-right: 10px;" /></a>
					</div>

				</div>

			</div><!-- /row -->

			<!-- Copyright -->
			<div class="row">
				<hr class="dark-hr">
				<div class="col-sm-11 col-xs-10">
					<p class="copyright" style="font-size: 10px;">© 2017 Find My Bicy. Todos los derechos reservados.</b></p>
				</div>
				<div class="col-sm-1 col-xs-2 text-right">
					<a href="#" class="scroll-top"><div class="footer-scrolltop-holder"><span class="ion-ios7-arrow-up footer-scrolltop"></span></div></a>
				</div>
			</div><!-- /row -->
		</div><!-- /container -->
	</footer><!-- /footer -->

	<input type="hidden" id="ocultoCarrusel" value="0" />
	<input type="hidden" id="ocultoCarruselDown" value="0" />


	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>

	<script type="text/javascript">

		function explode(){
			if ($("#ocultoCarrusel").get(0).value == "0"){
				$("#ocultoCarrusel").get(0).value = "1";
				$("#item1-carrusel").fadeOut(300, function(){
					$("#item2-carrusel").fadeIn(300);
				});
				$("#item1-carrusel-small").fadeOut(300, function(){
					$("#item2-carrusel-small").fadeIn(300);
				});
			} else {
				$("#ocultoCarrusel").get(0).value = "0";
				$("#item2-carrusel").fadeOut(300, function(){
					$("#item1-carrusel").fadeIn(300);
				});
				$("#item2-carrusel-small").fadeOut(300, function(){
					$("#item1-carrusel-small").fadeIn(300);
				});
			}
		  setTimeout(explode, 10000);
		}
		setTimeout(explode, 10000);







		function explodedown(){
			if ($("#ocultoCarruselDown").get(0).value == "0"){
				$("#ocultoCarruselDown").get(0).value = "1";
				$("#item1-carrusel-down").fadeOut(300, function(){
					$("#item2-carrusel-down").fadeIn(300);
				});
				/*$("#item1-carrusel-small").fadeOut(300, function(){
					$("#item2-carrusel-small").fadeIn(300);
				});*/
			} else {
				$("#ocultoCarruselDown").get(0).value = "0";
				$("#item2-carrusel-down").fadeOut(300, function(){
					$("#item1-carrusel-down").fadeIn(300);
				});
				/*$("#item2-carrusel-small").fadeOut(300, function(){
					$("#item1-carrusel-small").fadeIn(300);
				});*/
			}
		  setTimeout(explodedown, 10000);
		}
		setTimeout(explodedown, 10000);

	</script>

	<script type="text/javascript">
	jQuery(document).ready(function() {


		







		$("#img_logo_nav").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_over_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  }
		});

		$("#img_logo_nav_text").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_over_img.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_img.png');
		  }
		});


		$("#img_loggin").on({
		 "mouseover" : function() {
		    this.src = 'images/icon_user_jj.png';
		  },
		  "mouseout" : function() {
		    this.src='images/icon_user.png';
		  }
		});




		$(window).on({
		 "scroll" : function() {
		    //$("#div-nav").height(50);
		    $("div-nav").css("height", 50);
		  }
		});

	});
	</script>


</body>
</html>	