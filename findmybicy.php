<!DOCTYPE html>
<html>
<head>
	<title>Find My Bicy - Rodar nunca fue tan seguro</title>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Theme CSS -->
  	<link rel="stylesheet" type="text/css" href="admin/assets/skin/default_skin/css/theme.css">

  	<!-- Admin Forms CSS -->
  	<link rel="stylesheet" type="text/css" href="admin/assets/admin-tools/admin-forms/css/admin-forms.min.css">

	<style type="text/css">

		#btn-contacto-login:hover,
		#btn-contacto-login:focus{
			background:black;
			color: #53c5ea;
		}

		#btn-contacto-login{
			color: #FFF;
		}

	</style>

</head>
<body>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


	<!--navbar navbar-default -->
	<nav class="navbar  navbar-static-top" style="background: black; height: 40px;">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header" style="background: black;">
	      
	      <a class="navbar-brand" href="index.php" style="margin-top: -15px; margin-left: 20px;">
	      		<img src="images/logo_img.png" style="height: 40px; margin-top: 5px; margin-right: 20px; margin-bottom: 5px;" id="img_logo_nav" />
	      </a>
	    </div>




	    <div class="dropdown navbar-right" style="margin-right: 25px; margin-top: 10px; background: black;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="images/icon_hamburger.png" style="width: 30px;" /></a>
          <ul class="dropdown-menu" style="background: black; color: white;">
            <li><a id="btn-contacto-login" href="contacto.php">Contacto</a></li>
            <!--<li role="separator" class="divider"></li>-->
          </ul>
        </div>


	  </div><!-- /.container-fluid -->
	</nav>












	<!-- Login -->
	<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <!--<div class="modal-header" style="background: white; border-radius: 10px">
	                <button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	                <!--<h4 class="modal-title" id="loginLabel">Ingresa</h4>- ->
	                <!--<img src="images/logo_login.png" style="width: 135px;" />- ->
	            </div>-->
	            <!--<button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>-->
	            <div class="modal-body">
								<form role="form">
										<div class="form-group">
												<!--<label>Correo</label>-->
												<div class="input-group" style="width: 100%">
														<!--<div class="input-group-addon"><span class="ion-android-mail" style="font-size:9px;"></span></div>-->
														<input class="form-control" type="email" id="email" placeholder="Ingresa tu correo" style="width: 100%">
												</div>
										</div>
										<div class="form-group">
											<!--<label>Contraseña</label>-->
											<div class="input-group" style="width: 100%">
														<!--<div class="input-group-addon"><span class="ion-ios7-locked"></span></div>-->
													<input type="password" class="form-control" id="password" placeholder="Ingresa tu contraseña">
												</div>
										</div>
										<!--<div class="checkbox">
												<label>
													<input type="checkbox"> Recu&eacute;rdame
												</label>
										</div>-->

										<div class="row" style="padding-left: 20px; padding-right: 20px; padding-top: 5px; padding-bottom: 5px;">
											<span>¿No tienes cuenta? </span><a style="color: #53c5ea;" href="">Reg&iacute;strate</a>
										</div>

										<!--<hr class="mb20 mt15">
										<button type="submit" class="btn btn-rw btn-primary">Submit</button> &nbsp;&nbsp;&nbsp;<small><a href="#">Forgot your password?</a></small>-->
								</form><!-- /form -->
	            </div>
	            <div class="modal-footer" style="text-align: center;">
	                <button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>INGRESAR</b></button>
	            	</br>
	            	</br>
	            	<span>o</span>
	            	</br>
	            	</br>
	            	<a href=""><img src="images/icon_facebook_login.png" style="width: 40px; margin-right: 20px;" /></a>
	            	<a href=""><img src="images/icon_googleplus_login.png" style="width: 40px;" /></a>
	            </div>
	        </div><!-- /modal content -->
	    </div><!-- /modal dialog -->
	</div><!-- /modal holder -->
	<!-- End Login -->








	
	



	<!--<div id="carrusel" class="hidden-xs" style="height: 600px; width: 100%; text-align:center; justify-content: center; align-content: center;">
	  	<div id="item1-carrusel">
      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 600px; width: 100%;">
    	</div>
	    <div id="item2-carrusel" style="height: 600px; width: 100%; display: none; overflow: hidden;">
			<video loop="loop" style="width: 100%;" autoplay="autoplay">
		        <source src="videos/hunter.mp4" type="video/mp4"/>
		    </video>
	    </div>

	    <div style="margin-top: 200px;
    right: 0px;
    left: 0px;
    top: 0px;
    bottom: 0px;
    text-align:center; 
    justify-content: center; 
    align-content: center;
    width: 100%;
    height: 100px;
    padding: 10px;">
	    	<div style="color: white; background: transparent; font-size: 30px; font-family: Verdana; text-align: center;">
	    		<p style="color: white;  width: 100%; "><b>Rodar nunca fue tan seguro</b></p>
	  		</div>
	  		<div style="color: white; background: transparent; font-size: 15px; font-family: Verdana;">
	    		<p style="color: white; width: 100%;">Para <strong>todas las bicicletas</strong></p>
	  		</div>
	  		<div style="color: white; background: transparent; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
	    		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 18px; font-family: Arial, Gadget, sans-serif; " href="#"><b>Find</b> My Bicy &nbsp;<span class="glyphicon glyphicon-chevron-down"></span> </a>
	  		</div>
	    </div>


	    
	</div>-->




	





	<!-- Begin Content Section -->
	<section class="background-light-grey border-top" style="padding-bottom: 80px; padding-top: 60px; background: white;">
		<div class="container">
			<br>
			<div class="row mt40 mb40">

				<!-- Begin Carousel -->
				<div class="col-sm-6">
					<img src="" />
				</div><!-- /column -->
				<!-- End Carousel -->

				<!-- Content 1 -->
				<div class="col-sm-6 mt30-xs" style="padding-left: 100px;">
					<div class="panel mb25 mt5">
						<div class="panel-heading">
							<!---border-->
	                      <ul class="nav panel-tabs">
	                        <li class="active">
	                          <a href="#tab1_1" data-toggle="tab">Producto</a>
	                        </li>
	                        <li>
	                          <a href="#tab1_2" data-toggle="tab">Servicio</a>
	                        </li>
	                        <li>
	                          <a href="#tab1_3" data-toggle="tab">Descripción técnica</a>
	                        </li>
	                      </ul>
	                    </div>
	                    <div class="panel-body p20 pb10">
	                      <div class="tab-content pn br-n admin-form">
	                    	<div id="tab1_1" class="tab-pane active">
	                          <div class="section row">
	                            <div class="col-md-12">
	                              	<div class="heading"><h4>Producto</h4></div>
									<p>Dispositivo GPS antirrobo de alta precisión y cobertura satelital para diferentes tipos de bicicletas. El dispositivo se instala dentro del marco de la bicicleta, su batería recargable tiene una duración entre 4 y 10 semanas, dependiendo en nivel de uso.</p>
	                            </div>
	                          </div>
	                      	</div>
	                      	<div id="tab1_2" class="tab-pane">
	                          <div class="section row">
	                            <div class="col-md-12">
	                              	<div class="heading"><h4>Servicio</h4></div>
									<p>La comunicación de entre el GPS y el BicyUsuario se logra mediante el uso de la red de los operadores móviles, lo que implica un costo. El servicio se ofrece 24 Hrs al día, incluye localización de la bicicleta, reportes de robo a Familia, amigos, Policía Nacional, y Fiscalía General de la Nación, e información de ayuda a los BicyUsuarios. Esta comunicación es altamente cifrada y certificada por el operador móvil.</p>
	                            </div>
	                          </div>
	                      	</div>
	                      	<div id="tab1_3" class="tab-pane">
	                          <div class="section row">
	                            <div class="col-md-12">
	                              	<div class="heading"><h4>Descripción técnica</h4></div>
									<p>El producto cuenta con garantía de 6 meses frente a problemas de fabricación, y a fallas a la resistencia IP66. La batería es de 4.200 mAh, y el margen de error de la georeferenciación es de 2,5 metros, pues rectifica la ubicación con diferentes satélites. El diseño del protocolo de comunicación permite rápida comunicación y mejor cobertura en zonas remotas. El producto ofrece mayor cobertura mediante un servicio conectado a el doble de antenas satelitales, por una valor adicional.</p>
	                            </div>
	                          </div>
	                      	</div>
	                      </div>
	                    </div>
					</div>
						


					
				</div><!-- /column -->
				<!-- End Content 1 -->
			</div><!-- /row-->
		</div><!-- /container -->
	</section><!-- /section -->
	<!-- End Content Section -->






	






	<!-- Begin Content Section -->
	<section class="background-light-grey border-top hidden-xs" style="padding-bottom: 80px; padding-top: 80px; background: #EEEEEE;">
		<div class="container">
			<div class="row" style="text-align: center">
				<h1 style="color: #53c5ea">¿Cómo empezar?</h1>
				</br>
				<h5>Disponible para <b>todas las bicicletas</b></h5>
			</div>
			<br>
			<div class="row mt40 mb40">

				<!-- Begin Carousel -->
				<div class="col-sm-4" style="text-align: center;">
					<img src="images/paso1.png" style="height: 200px;" />
					</br>
					</br>
					</br>
					</br>
					<h5><a>Adquirir GPS</a> y pagar servicio.</h5>
					</br>
					</br>
					<h6 style="font-size: 11px">El costo periodico se deriva del operador del servicio</h6>

				</div><!-- /column -->
				<!-- End Carousel -->

				<!-- Content 1 -->
				<div class="col-sm-4 mt30-xs" style="text-align: center;">
					<img src="images/paso2.png" style="height: 200px;" />
					</br>
					</br>
					</br>
					</br>
					<h5>Completar registro e instalar GPS en la bicicleta.</h5>
					</br>
					</br>
					<h6 style="font-size: 11px">Ver video instructivo en cuenta de usuario</h6>

				</div><!-- /column -->

				<div class="col-sm-4 mt30-xs" style="text-align: center;">
					<img src="images/paso3.png" style="height: 200px;" />
					</br>
					</br>
					</br>
					</br>
					<h5><a>Accede en plataforma web</a> o descarga nuestro aplicativo móvil.</h5>
					</br>
					</br>
					<div class="row" style="text-align: center">
						<img src="images/logo-appstore.png" style="width: 120px;" />
						<img src="images/logo-googleplay.png" style="width: 120px;" />
					</div>
					
					

				</div><!-- /column -->

				<!-- End Content 1 -->
			</div><!-- /row-->
		</div><!-- /container -->
	</section><!-- /section -->
	<!-- End Content Section -->

	<!-- Begin Content Section -->
	<section class="background-light-grey border-top hidden-sm hidden-md hidden-lg hidden-xl" style="padding-bottom: 80px; padding-top: 80px; background: #EEEEEE;">
		<div class="container">
			<div class="row" style="text-align: center">
				<h1 style="color: #53c5ea">¿Cómo empezar?</h1>
				<h5>Disponible para <b>todas las bicicletas</b></h5>
			</div>
			<br>
			<div class="row mt40 mb40">

				<!-- Begin Carousel -->
				<div class="col-sm-4" style="text-align: center;">
					<img src="images/paso1.png" style="height: 200px;" />
					</br>
					<h5><a>Adquirir GPS</a> y pagar servicio.</h5>
					<h6 style="font-size: 11px">El costo periodico se deriva del operador del servicio</h6>
					</br>
				</div><!-- /column -->
				<!-- End Carousel -->

				<!-- Content 1 -->
				<div class="col-sm-4 mt30-xs" style="text-align: center;">
					<img src="images/paso2.png" style="height: 200px;" />
					</br>
					<h5>Completar registro e instalar GPS en la bicicleta.</h5>
					<h6 style="font-size: 11px">Ver video instructivo en cuenta de usuario</h6>
					</br>
				</div><!-- /column -->

				<div class="col-sm-4 mt30-xs" style="text-align: center;">
					<img src="images/paso3.png" style="height: 200px;" />
					</br>
					<h5><a>Accede en plataforma web</a> o descarga nuestro aplicativo móvil.</h5>
					</br>
					<div class="row" style="text-align: center">
						<img src="images/logo-appstore.png" style="width: 120px;" />
						<img src="images/logo-googleplay.png" style="width: 120px;" />
					</div>
					
					

				</div><!-- /column -->

				<!-- End Content 1 -->
			</div><!-- /row-->
		</div><!-- /container -->
	</section><!-- /section -->
	<!-- End Content Section -->
























	<!-- Login -->
	<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <!--<div class="modal-header" style="background: white; border-radius: 10px">
	                <button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	                <!--<h4 class="modal-title" id="loginLabel">Ingresa</h4>- ->
	                <!--<img src="images/logo_login.png" style="width: 135px;" />- ->
	            </div>-->
	            <!--<button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>-->
	            <div class="modal-body">
								<form role="form">
										<div class="form-group">
												<!--<label>Correo</label>-->
												<div class="input-group" style="width: 100%">
														<!--<div class="input-group-addon"><span class="ion-android-mail" style="font-size:9px;"></span></div>-->
														<input class="form-control" type="email" id="email" placeholder="Ingresa tu correo" style="width: 100%">
												</div>
										</div>
										<div class="form-group">
											<!--<label>Contraseña</label>-->
											<div class="input-group" style="width: 100%">
														<!--<div class="input-group-addon"><span class="ion-ios7-locked"></span></div>-->
													<input type="password" class="form-control" id="password" placeholder="Ingresa tu contraseña">
												</div>
										</div>
										<!--<div class="checkbox">
												<label>
													<input type="checkbox"> Recu&eacute;rdame
												</label>
										</div>-->

										<div class="row" style="padding-left: 20px; padding-right: 20px; padding-top: 5px; padding-bottom: 5px;">
											<span>¿No tienes cuenta? </span><a style="color: #53c5ea;" href="">Reg&iacute;strate</a>
										</div>

										<!--<hr class="mb20 mt15">
										<button type="submit" class="btn btn-rw btn-primary">Submit</button> &nbsp;&nbsp;&nbsp;<small><a href="#">Forgot your password?</a></small>-->
								</form><!-- /form -->
	            </div>
	            <div class="modal-footer" style="text-align: center;">
	                <button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>INGRESAR</b></button>
	            	</br>
	            	</br>
	            	<span>o</span>
	            	</br>
	            	</br>
	            	<a href=""><img src="images/icon_facebook_login.png" style="width: 40px; margin-right: 20px;" /></a>
	            	<a href=""><img src="images/icon_googleplus_login.png" style="width: 40px;" /></a>
	            </div>
	        </div><!-- /modal content -->
	    </div><!-- /modal dialog -->
	</div><!-- /modal holder -->
	<!-- End Login -->








	





	<section class="row" style="margin-bottom: 50px; margin-top: 30px;">
		<div class="row">
			<div class="col-md-6" style="text-align: center; padding-top: 30px; padding-left: 100px; padding-right: 100px;">
				<div class="panel-body" style="border: none;">
			        <form role="form">
			        	<div class="form-group">
			                <input type="text" class="form-control" id="nombres" placeholder="Nombres">
			            </div>
			            <div class="form-group">
			                <input type="text" class="form-control" id="apellidos" placeholder="Apellidos">
			            </div>
	      				<div class="form-group">
			                <input class="form-control" type="text" placeholder="N&uacute;mero celular">
			            </div>

			        </form>
			    </div>
			</div>
			<div class="col-md-6" style="text-align: center; padding-top: 30px; padding-left: 100px; padding-right: 100px;">
				<div class="panel-body" style="border: none;">
			        <form role="form">
			            <div class="form-group">
			                <input class="form-control" type="text" placeholder="E-Mail">
			            </div>
			            <div class="form-group">
			            	<input type="password" class="form-control" placeholder="Contraseña">
			            </div>
			            <div class="form-group">
			            	<input type="password" class="form-control" placeholder="Confirma Contraseña">
			            </div>
			            </br>
						</br>
			        </form>
			    </div>
			</div>
		</div>
			

		<div class="row" style="text-align: center;">
			<div class="checkbox">
                <label>
                  <input type="checkbox"> He leido y acepto los <a style="color: #53c5ea;">términos y condiciones</a>.
                </label>
            </div>
            </br>
            <a href="mapa.php" class="btn btn-rw btn-primary" style="background: #53c5ea;"><b>REGÍSTRATE</b></a>
            </br>
        	</br>
        	<div class="row" style="padding-left: 20px; padding-right: 20px; padding-top: 5px; padding-bottom: 5px;">
				<span>¿Ya tienes cuenta? </span><a style="color: #53c5ea;" href="" data-toggle="modal" data-target="#login">Ingresa</a>
			</div>
		</div>
		<!--<div class="col-md-6" style="text-align: center; padding-top: 30px; padding-left: 100px; padding-right: 100px;">

			<div class="col-md-12" style="text-align: center;">
				<a href=""><img src="images/icon_facebook_login.png" style="width: 40px; margin-right: 20px;" /></a>
	            <a href=""><img src="images/icon_googleplus_login.png" style="width: 40px;" /></a>
			</div>
			</br>
			</br>
			</br>
			</br>
			<hr class="mb20 mt20">
			</br>
			<div class="panel-body" style="border: none;">
		        <form role="form">
		            <div class="form-group">
		                <input class="form-control" type="email" placeholder="E-Mail">
		            </div>
		            <div class="form-group">
		            	<input type="password" class="form-control" id="pass" placeholder="Contraseña">
		            </div>
		            <a href="mapa.php" class="btn btn-rw btn-primary" style="background: #53c5ea;"><b>INGRESAR</b></a>
		        </form>
		    </div>
		</div>-->
	</section>







	<!-- Begin Footer -->
	<footer class="footer" style="padding-top: 80px; background: black;">
		<div class="container">
			<div class="row">

				<!-- Social -->
				<!--<div class="col-sm-4 mg25-xs">
					<!--<div class="heading-footer"><h4>Redes Sociales</h4></div>- ->
						<span href="shop-single-product.html" class="fa fa-facebook bordered-icon-dark bordered-icon-sm mb5"></span>
						<span class="fa fa-instagram bordered-icon-dark bordered-icon-sm mb5 mt10"></span>
						<span class="fa fa-youtube bordered-icon-dark bordered-icon-sm mb5"></span>
						</br>
						</br>
						<div class="row" style="text-align: left">
							<img src="images/logo-appstore.png" style="width: 120px;" />
							<img src="images/logo-googleplay.png" style="width: 120px;" />
						</div>
				</div>-->

				<!-- Contact -->
				<div class="col-sm-6 col-xs-6 mg25-xs" style="padding-left: 40px; font-size: 15px;">
					<!--<div class="heading-footer"><h4 style="color: #53c5ea">Find My Bicy</h4></div>-->
					<p><span style="color: white" class="glyphicon glyphicon-home"></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;¿Quiénes somos?</a></small></p>
					<p><span style="color: white" class="glyphicon glyphicon-bullhorn"></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;TESTIMONIOS</a></small></p>
					<p><span style="color: white" class="glyphicon glyphicon-shopping-cart"></span><small class="address">&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: white;" href="">Puntos de venta</a></small></p>
					<p><span style="color: white" class="glyphicon glyphicon-earphone"></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;Contacto</a></small></p>
					</br>
					<p></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;Términos y condiciones</a></small></p>
					<p></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;Política de privacidad</a></small></p>
					<!--<p><span class="ion-home footer-info-icons"></span><small class="address">Calle 00 # 00-00 Bogot&aacute;</small></p>
					<p><span class="ion-email footer-info-icons"></span><small class="address"><a href="mailto:info@findmybicy.com">info@findmybicy.com</a></small></p>
					<p><span class="ion-ios7-telephone footer-info-icons"></span><small class="address">+573156607087</small></p>-->
				</div>


				<div class="col-sm-6 col-xs-6 mg25-xs">
					<div class="row" style="text-align: center;">
						<a href="" style="margin-right: 10px;"><img src="images/icon_facebook.png" style="width: 40px;" /></a>
						<a href="" style="margin-right: 10px;"><img src="images/icon_instagram.png" style="width: 40px;" /></a>
						<a href="" style="margin-right: 10px;"><img src="images/icon_youtube.png" style="width: 40px;" /></a>
					</div>
					
					</br>
					<!--bordered-icon-dark bordered-icon-sm mb5-->
					<!--<span href="" class="fa fa-facebook" style="height: 100px;"></span>
					<span class="fa fa-instagram bordered-icon-dark bordered-icon-sm mb5 mt10"></span>
					<span class="fa fa-youtube bordered-icon-dark bordered-icon-sm mb5"></span>-->
					</br>
					</br>
					</br>
					<div class="row" style="text-align: center;">
						<a href=""><img src="images/logo-appstore.png" style="width: 120px; margin-right: 10px;" /></a>
						<a href=""><img src="images/logo-googleplay.png" style="width: 120px; margin-right: 10px;" /></a>
					</div>

				</div>

			</div><!-- /row -->

			<!-- Copyright -->
			<div class="row">
				<hr class="dark-hr">
				<div class="col-sm-11 col-xs-10">
					<p class="copyright" style="font-size: 10px;">© 2017 Find My Bicy. Todos los derechos reservados.</b></p>
				</div>
				<div class="col-sm-1 col-xs-2 text-right">
					<a href="#" class="scroll-top"><div class="footer-scrolltop-holder"><span class="ion-ios7-arrow-up footer-scrolltop"></span></div></a>
				</div>
			</div><!-- /row -->
		</div><!-- /container -->
	</footer><!-- /footer -->

	<input type="hidden" id="ocultoCarrusel" value="0" />
	<input type="hidden" id="ocultoCarruselDown" value="0" />


	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>

	<script type="text/javascript">

		function explode(){
			if ($("#ocultoCarrusel").get(0).value == "0"){
				$("#ocultoCarrusel").get(0).value = "1";
				$("#item1-carrusel").fadeOut(300, function(){
					$("#item2-carrusel").fadeIn(300);
				});
				$("#item1-carrusel-small").fadeOut(300, function(){
					$("#item2-carrusel-small").fadeIn(300);
				});
			} else {
				$("#ocultoCarrusel").get(0).value = "0";
				$("#item2-carrusel").fadeOut(300, function(){
					$("#item1-carrusel").fadeIn(300);
				});
				$("#item2-carrusel-small").fadeOut(300, function(){
					$("#item1-carrusel-small").fadeIn(300);
				});
			}
		  setTimeout(explode, 10000);
		}
		setTimeout(explode, 10000);







		function explodedown(){
			if ($("#ocultoCarruselDown").get(0).value == "0"){
				$("#ocultoCarruselDown").get(0).value = "1";
				$("#item1-carrusel-down").fadeOut(300, function(){
					$("#item2-carrusel-down").fadeIn(300);
				});
				/*$("#item1-carrusel-small").fadeOut(300, function(){
					$("#item2-carrusel-small").fadeIn(300);
				});*/
			} else {
				$("#ocultoCarruselDown").get(0).value = "0";
				$("#item2-carrusel-down").fadeOut(300, function(){
					$("#item1-carrusel-down").fadeIn(300);
				});
				/*$("#item2-carrusel-small").fadeOut(300, function(){
					$("#item1-carrusel-small").fadeIn(300);
				});*/
			}
		  setTimeout(explodedown, 10000);
		}
		setTimeout(explodedown, 10000);

	</script>

	<script type="text/javascript">
	jQuery(document).ready(function() {


		







		$("#img_logo_nav").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_over_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  }
		});

		$("#img_logo_nav_text").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_over_img.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_img.png');
		  }
		});


		$("#img_loggin").on({
		 "mouseover" : function() {
		    this.src = 'images/icon_user_jj.png';
		  },
		  "mouseout" : function() {
		    this.src='images/icon_user.png';
		  }
		});




		$(window).on({
		 "scroll" : function() {
		    //$("#div-nav").height(50);
		    $("div-nav").css("height", 50);
		  }
		});

	});
	</script>


</body>
</html>	