<!DOCTYPE html>
<html>
<head>
	<title>Find My Bicy - Rodar nunca fue tan seguro</title>
		
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">

    	<!--<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />-->
    	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

		<!-- Datatables CSS -->
  		<link rel="stylesheet" type="text/css" href="vendor/plugins/datatables/media/css/dataTables.bootstrap.css">

  

	  	<!-- Font CSS (Via CDN) -->
	  	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>


    <!-- FullCalendar Plugin CSS -->
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/magnific/magnific-popup.css">

  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/tagmanager/tagmanager.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/daterange/daterangepicker.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/colorpicker/css/bootstrap-colorpicker.min.css">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Theme CSS -->
  	<link rel="stylesheet" type="text/css" href="admin/assets/skin/default_skin/css/theme.css">

  	<!-- Admin Forms CSS -->
  	<link rel="stylesheet" type="text/css" href="admin/assets/admin-tools/admin-forms/css/admin-forms.min.css">

  	<!-- Favicon -->
  <link rel="shortcut icon" href="admin/assets/img/favicon.ico">

	<style type="text/css">
		#btn-findmybicy:hover,
		#btn-findmybicy:focus{
			background-color:transparent;
		}

		#btn-contacto:hover,
		#btn-contacto:focus{
			background-color:transparent;
		}

		#btn-iniciosesion:hover,
		#btn-iniciosesion:focus{
			background-color:transparent;
		}

		#btn-contacto-login:hover,
		#btn-contacto-login:focus{
			background:black;
			color: #53c5ea;
		}

		#btn-contacto-login{
			color: #FFF;
		}

		html, body {
	        height: 100%;
	        margin: 0;
	        padding: 0;
	    }

	    .boton-file {
	    	background-image: "images/icon_file.png";

	    }

	</style>

</head>
<body>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


	<!--navbar navbar-default -->
	<nav class="navbar  navbar-static-top" style="background: black; height: 40px;">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header" style="background: black;">
	      
	      <a class="navbar-brand" href="index.php" style="margin-top: -15px; margin-left: 20px;">
	      		<img src="images/logo_img.png" style="height: 40px; margin-top: 5px; margin-right: 20px; margin-bottom: 5px;" id="img_logo_nav" />
	      </a>
	    </div>




	    <div class="dropdown navbar-right" style="margin-right: 25px; margin-top: 10px; background: black;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="images/icon_hamburger.png" style="width: 30px;" /></a>
          <ul class="dropdown-menu" style="background: black; color: white;">
          	<li><a id="btn-contacto-login" href="perfil.php">Perfil</a></li>
          	<li><a id="btn-contacto-login" href="">Scott 740</a></li>
          	<li><a id="btn-contacto-login" href="" data-toggle="modal" data-target="#nueva_bicy">Añadir Bicy</a></li>
            <li><a id="btn-contacto-login" href="contacto.php">Contacto</a></li>

            <!--<li role="separator" class="divider"></li>-->
          </ul>
        </div>


	  </div><!-- /.container-fluid -->
	</nav>












	










	<div class="modal fade" id="nueva_bicy" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
					<form role="form">
						<div class="form-group">
								<div class="input-group" style="width: 100%">
										<input class="form-control" type="text" id="txtCodigoNuevaBicy" placeholder="Código" style="width: 100%">
								</div>
						</div>
						<div class="form-group">
							<div class="input-group" style="width: 100%">
									<input type="text" class="form-control" id="txtNombreNuevaBicy" placeholder="Nombre Bicy">
								</div>
						</div>
						<div class="form-group">
							<div class="input-group ">
                            	<div class="col-md-4">
                            		
                            		<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
				                        <div class="fileupload-preview thumbnail mb20">
				                        	<!--data-src="holder.js/178%x147"-->
				                          <img src="images/icon_placeholder.png">
				                        </div>
				                        <div class="row">
				                          <div class="col-xs-12 pr5">
				                            <label for="name2" class="field-icon"></label>
				                          </div>
				                          <div class="col-xs-12">
				                          	<!--button btn-system-->
				                            <span class="btn-file btn-block">
				                              <!--<span class="fileupload-new">Select</span>
				                              <span class="fileupload-exists">Change</span>-->
				                              <input type="file">
				                            </span>
				                          </div>
				                        </div>
				                    </div>

                            	</div>
                            	<div class="col-md-4">
                            		
                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
				                        <div class="fileupload-preview thumbnail mb20">
				                          <img src="images/icon_placeholder.png">
				                        </div>
				                        <div class="row">
				                          <div class="col-xs-12 pr5">
				                            <label for="name2" class="field-icon"></label>
				                          </div>
				                          <div class="col-xs-12">
				                          	<!--button btn-system-->
				                            <span class="btn-file btn-block">
				                              <!--<span class="fileupload-new">Select</span>
				                              <span class="fileupload-exists">Change</span>-->
				                              <input type="file">
				                            </span>
				                          </div>
				                        </div>
				                    </div>

                            	</div>
                            	<div class="col-md-4">

                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
				                        <div class="fileupload-preview thumbnail mb20">
				                          <img src="images/icon_placeholder.png">
				                        </div>
				                        <div class="row">
				                          <div class="col-xs-12 pr5">
				                            <label for="name2" class="field-icon"></label>
				                          </div>
				                          <div class="col-xs-12">
				                          	<!--button btn-system-->
				                            <span class="btn-file btn-block">
				                              <!--<span class="fileupload-new">Select</span>
				                              <span class="fileupload-exists">Change</span>-->
				                              <input type="file">
				                            </span>
				                          </div>
				                        </div>
				                    </div>

                            	</div>
							</div>
						</div>
							
						<button id="btn_anhadir_bicy" type="button" data-dismiss="modal" data-toggle="modal" data-target="#planes" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>AÑADIR</b></button>
						</br>

					</form>
	            </div>
	        </div>
	    </div>
	</div>









	<div class="modal fade" id="planes" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:1000px; margin-left:-500px; height:1000px; margin-top:-200px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
					<form role="form">

						<div class="row" style="text-align: center;">
							<div class="row">
								<div class="col-md-3" style="text-align: center;">
									<h2>PLAN MES</h2>
									<h4>* Caracteristica 1</h4>
									<h4>* Caracteristica 2</h4>
									<h4>* Caracteristica 3</h4>
									<h4>* Caracteristica 4</h4>
									<h4>* Caracteristica 5</h4>
									<h4>* Caracteristica 6</h4>
									<h1 style="color: black;">$20.000 COP</h1>
									<button id="btn_plan_1" type="button" data-dismiss="modal" data-toggle="modal" data-target="#credit-card" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>SELECCIONAR</b></button>
									</br>
								</div>

								<div class="col-md-3" style="text-align: center;">
									<h2>PLAN TRIMESTRE</h2>
									<h4>* Caracteristica 1</h4>
									<h4>* Caracteristica 2</h4>
									<h4>* Caracteristica 3</h4>
									<h4>* Caracteristica 4</h4>
									<h4>* Caracteristica 5</h4>
									<h4>* Caracteristica 6</h4>
									<h1 style="color: black;">$40.000 COP</h1>
									<button id="btn_plan_2" type="button" data-dismiss="modal" data-toggle="modal" data-target="#credit-card" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>SELECCIONAR</b></button>
									</br>
								</div>

								<div class="col-md-3" style="text-align: center;">
									<h2>PLAN SEMESTRE</h2>
									<h4>* Caracteristica 1</h4>
									<h4>* Caracteristica 2</h4>
									<h4>* Caracteristica 3</h4>
									<h4>* Caracteristica 4</h4>
									<h4>* Caracteristica 5</h4>
									<h4>* Caracteristica 6</h4>
									<h1 style="color: black;">$60.000 COP</h1>
									<button id="btn_plan_3" type="button" data-dismiss="modal" data-toggle="modal" data-target="#credit-card" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>SELECCIONAR</b></button>
									</br>
								</div>

								<div class="col-md-3" style="text-align: center;">
									<h2>PLAN AÑO</h2>
									<h4>* Caracteristica 1</h4>
									<h4>* Caracteristica 2</h4>
									<h4>* Caracteristica 3</h4>
									<h4>* Caracteristica 4</h4>
									<h4>* Caracteristica 5</h4>
									<h4>* Caracteristica 6</h4>
									<h1 style="color: black;">$80.000 COP</h1>
									<button id="btn_plan_3" type="button" data-dismiss="modal" data-toggle="modal" data-target="#credit-card" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>SELECCIONAR</b></button>
									</br>
								</div>
								</br>
							</div>
							</br>
							<h6></h6>
							<h6>* Al seleccionar el plan acepto los términos y condiciones.....</h6>
						</div>
						
					</form>
	            </div>
	        </div>
	    </div>
	</div>











	<div class="modal fade" id="credit-card" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:350px; margin-left:-175px; height:350px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
					<form role="form">
						
						<div class="col-md-12" style="text-align: center;">
							<input type="text" id="txt_nombre_tarjeta" name="txt_nombre_tarjeta" placeholder="Nombre en la tarjeta" class="form-control" />
							</br>
							<input type="text" id="txt_numero_tarjeta" name="txt_numero_tarjeta" placeholder="Número tarjeta" maxlength="19" class="form-control" />
							</br>
							<div class="col-md-4">
								<select id="sl_mes" name="sl_mes" class="form-control">
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select>
								</br>
							</div>
							<div class="col-md-4">
								<select id="sl_anho" name="sl_anho" class="form-control">
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
									<option value="2024">2024</option>
									<option value="2025">2025</option>
									<option value="2026">2026</option>
									<option value="2027">2027</option>
									<option value="2028">2028</option>
									<option value="2029">2029</option>
									<option value="2030">2030</option>
								</select>
								</br>
							</div>
							<div class="col-md-4">
								<input type="text" id="txt_cvc_tarjeta" name="txt_cvc_tarjeta" placeholder="CVC" class="form-control" />
								</br>
							</div>
							<button id="btn_pagar" type="button" data-dismiss="modal" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>PAGAR</b></button>
							</br>
						</div>

						
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>

					</form>
	            </div>
	        </div>
	    </div>
	</div>












	<div class="modal fade" id="usuarios-dispositivo" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:800px; margin-left:-400px; height:800px; margin-top:-175px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
					<form role="form">
						
						<div class="col-md-12" style="text-align: center;">

							<table class="table table-responsive">
								<tr>
									<td>Usuario</td>
									<td>Puede</td>
									<td style="width: 70px;">Opciones</td>
								</tr>
								<tr>
									<td>
										<input type="text" id="txt_nombre_usuario" name="txt_nombre_usuario" placeholder="Nombre" class="form-control" />
									</td>
									<td><select id="sl_mes" name="sl_mes" class="form-control">
											<option value="0">Seleccionar</option>
											<option value="1">Administrar</option>
											<option value="2">Ver</option>
										</select>
									</td>
									<td>
										<button id="btn_add_usuario" type="button" data-dismiss="modal" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>AÑADIR</b></button>
									</td>
								</tr>
								<tr>
									<td>Juan Jose Cuenca (DUEÑO)</td>
									<td>Administrar</td>
									<td></td>
								</tr>
								<tr>
									<td>Jonathan Fajardo Roa</td>
									<td>Administrar</td>
									<td>
										<button id="btn_eliminar_usuario" type="button" data-dismiss="modal" class="btn btn-rw btn-danger" style="width: 100%; background: #53c5ea;"><b>ELIMINAR</b></button>
										<button id="btn_actualizar_usuario" type="button" data-dismiss="modal" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>ACTUALIZAR</b></button>
									</td>
								</tr>
								<tr>
									<td>Juanito Perez</td>
									<td>Ver</td>
									<td>
										<button id="btn_eliminar_usuario" type="button" data-dismiss="modal" class="btn btn-rw btn-danger" style="width: 100%; background: #53c5ea;"><b>ELIMINAR</b></button>
										<button id="btn_actualizar_usuario" type="button" data-dismiss="modal" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>ACTUALIZAR</b></button>
									</td>
								</tr>
							</table>

							
							</br>
							</br>
						</div>

						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>


					</form>
	            </div>
	        </div>
	    </div>
	</div>





	
	



	<div class="row" style="background: white;">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<!--<div class="row">
				<h2 style="color: #53c5ea;">Mi Perfil</h2>
			</div>-->
			</br>
			</br>
			</br>
			<div class="row">
				<div class="col-md-3">
					<h4>Información personal</h4>

				</div>
				<div class="col-md-6">
					<div class="col-md-4">
						
					</div>
					<div class="col-md-4">
						<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
	                        <div class="fileupload-preview thumbnail mb20">
	                        	<!--data-src="holder.js/178%x147"-->
	                          <img src="images/icon_placeholder.png">
	                        </div>
	                        <div class="row">
	                          <div class="col-xs-12 pr5">
	                            <label for="name2" class="field-icon"></label>
	                          </div>
	                          <div class="col-xs-12">
	                          	<!--button btn-system-->
	                            <span class="btn-file btn-block">
	                              <!--<span class="fileupload-new">Select</span>
	                              <span class="fileupload-exists">Change</span>-->
	                              <input type="file">
	                            </span>
	                          </div>
	                        </div>
	                    </div>
					</div>
					<div class="col-md-4">
						
					</div>
					<div class="row">
						<input type="text" id="txtNombre" name="txtNombre" class="form-control" placeholder="Nombre" />
						</br>
						<input type="text" id="txtApellido" name="txtApellido" class="form-control" placeholder="Apellido" />
						</br>
						<input type="text" id="txtTelefono" name="txtTelefono" class="form-control" placeholder="Teléfono" />
						</br>
						<input type="text" id="txtCorreo" name="txtCorreo" class="form-control" placeholder="Correo electrónico" />
						</br>
						<input type="password" id="txtClave" name="txtClave" class="form-control" placeholder="Contraseña" />
						</br>
						<input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="ACTUALIZAR DATOS" />
					</div>
				</div>
			</div>
			</br>
			</br>
			</br>
			<div class="row">
				<!--<div class="col-md-3">
					<h4>Mis Dispositivos</h4>
				</div>-->
				<div class="col-md-12">
					<div class="row">
						<table class="table table-responsive">
							<tr>
								<td>ID</td>
								<td>NOMBRE</td>
								<td>IMAGENES</td>
								<td>PLAN</td>
								<td>ESTADO</td>
								<td>OPCIONES</td>
							</tr>
							<tr>
								<td>8217382138</td>
								<td>SEMI CARRERA 1</td>
								<td style="width: 400px; ">
									<div class="col-md-2">
	                            		
	                            		<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                        	<!--data-src="holder.js/178%x147"-->
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">
	                            		
	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">

	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
								</td>
								<td>
									MENSUAL</br>
									MASTERCARD **** **** **** 4534
									<input type="button" class="btn btn-rw btn-warning" style="background: #53c5ea;" value="EDITAR" />
								</td>
								<td>EN REPOSO</td>
								<td>
									<a data-toggle="modal" data-target="#usuarios-dispositivo"><input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="USUARIOS" /></a>
									<input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="DETALLE" />
								</td>
							</tr>




							<tr>
								<td>8217382138</td>
								<td>SEMI CARRERA 1</td>
								<td style="width: 400px; ">
									<div class="col-md-2">
	                            		
	                            		<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                        	<!--data-src="holder.js/178%x147"-->
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">
	                            		
	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">

	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
								</td>
								<td>
									MENSUAL</br>
									MASTERCARD **** **** **** 4534
									<input type="button" class="btn btn-rw btn-warning" style="background: #53c5ea;" value="EDITAR" />
								</td>
								<td>EN REPOSO</td>
								<td>
									<a data-toggle="modal" data-target="#usuarios-dispositivo"><input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="USUARIOS" /></a>
									<input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="DETALLE" />
								</td>
							</tr>


							<tr>
								<td>8217382138</td>
								<td>SEMI CARRERA 1</td>
								<td style="width: 400px; ">
									<div class="col-md-2">
	                            		
	                            		<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                        	<!--data-src="holder.js/178%x147"-->
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">
	                            		
	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">

	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
								</td>
								<td>
									MENSUAL</br>
									MASTERCARD **** **** **** 4534
									<input type="button" class="btn btn-rw btn-warning" style="background: #53c5ea;" value="EDITAR" />
								</td>
								<td>EN REPOSO</td>
								<td>
									<a data-toggle="modal" data-target="#usuarios-dispositivo"><input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="USUARIOS" /></a>
									<input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="DETALLE" />
								</td>
							</tr>


							<tr>
								<td>8217382138</td>
								<td>SEMI CARRERA 1</td>
								<td style="width: 400px; ">
									<div class="col-md-2">
	                            		
	                            		<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                        	<!--data-src="holder.js/178%x147"-->
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">
	                            		
	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">

	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
								</td>
								<td>
									MENSUAL</br>
									MASTERCARD **** **** **** 4534
									<input type="button" class="btn btn-rw btn-warning" style="background: #53c5ea;" value="EDITAR" />
								</td>
								<td>EN REPOSO</td>
								<td>
									<a data-toggle="modal" data-target="#usuarios-dispositivo"><input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="USUARIOS" /></a>
									<input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="DETALLE" />
								</td>
							</tr>

							<tr>
								<td>8217382138</td>
								<td>SEMI CARRERA 1</td>
								<td style="width: 400px; ">
									<div class="col-md-2">
	                            		
	                            		<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                        	<!--data-src="holder.js/178%x147"-->
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">
	                            		
	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
	                            	<div class="col-md-2">

	                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail mb20">
					                          <img src="images/icon_placeholder.png">
					                        </div>
					                        <div class="row">
					                          <div class="col-xs-12 pr5">
					                            <label for="name2" class="field-icon"></label>
					                          </div>
					                          <div class="col-xs-12">
					                          	<!--button btn-system-->
					                            <span class="btn-file btn-block">
					                              <!--<span class="fileupload-new">Select</span>
					                              <span class="fileupload-exists">Change</span>-->
					                              <input type="file">
					                            </span>
					                          </div>
					                        </div>
					                    </div>

	                            	</div>
								</td>
								<td>
									MENSUAL</br>
									MASTERCARD **** **** **** 4534
									<input type="button" class="btn btn-rw btn-warning" style="background: #53c5ea;" value="EDITAR" />
								</td>
								<td>EN REPOSO</td>
								<td>
									<a data-toggle="modal" data-target="#usuarios-dispositivo"><input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="USUARIOS" /></a>
									<input type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;" value="DETALLE" />
								</td>
							</tr>
							
						</table>
					</div>
				</div>
			</div>
			</br>
			</br>
			</br>
			<!--<div class="row">
				<div class="col-md-3">
					<h4>Metodo de Pago</h4>
				</div>
				<div class="col-md-9">
					<div class="row">
						<form role="form">
						
							<div class="col-md-6" style="text-align: center;">
								<input type="text" id="txt_nombre_tarjeta" name="txt_nombre_tarjeta" placeholder="Nombre en la tarjeta" class="form-control" />
								</br>
								<input type="text" id="txt_numero_tarjeta" name="txt_numero_tarjeta" placeholder="Número tarjeta" maxlength="19" class="form-control" />
								</br>
								<div class="col-md-4">
									<select id="sl_mes" name="sl_mes" class="form-control">
										<option value="01">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									</br>
								</div>
								<div class="col-md-4">
									<select id="sl_anho" name="sl_anho" class="form-control">
										<option value="2010">2010</option>
										<option value="2011">2011</option>
										<option value="2012">2012</option>
										<option value="2013">2013</option>
										<option value="2014">2014</option>
										<option value="2015">2015</option>
										<option value="2016">2016</option>
										<option value="2017">2017</option>
										<option value="2018">2018</option>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
										<option value="2023">2023</option>
										<option value="2024">2024</option>
										<option value="2025">2025</option>
										<option value="2026">2026</option>
										<option value="2027">2027</option>
										<option value="2028">2028</option>
										<option value="2029">2029</option>
										<option value="2030">2030</option>
									</select>
									</br>
								</div>
								<div class="col-md-4">
									<input type="text" id="txt_cvc_tarjeta" name="txt_cvc_tarjeta" placeholder="CVC" class="form-control" />
									</br>
								</div>
								<button type="button" class="btn btn-rw btn-primary" style="background: #53c5ea;"><b>ACTUALIZAR DATOS DE PAGO</b></button>
								</br>
							</div>

						</form>
					</div>
				</div>
			</div>
			</br>
			</br>
			</br>
			<div class="row">
				<div class="col-md-3">
					<h4>Ayudas</h4>
				</div>
				<div class="col-md-9">
					<div class="row">
						
					</div>
				</div>
			</div>-->
			</br>
			</br>
			</br>

		</div>
		<div class="col-md-1"></div>
	</div>














	

	<input type="hidden" id="ocultoCarrusel" value="0" />
	<input type="hidden" id="ocultoCarruselDown" value="0" />


	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>




	<!-- jQuery -->
  <script src="admin/vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="admin/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- HighCharts Plugin -->
  <script src="admin/vendor/plugins/highcharts/highcharts.js"></script>

  <!-- JvectorMap Plugin + US Map (more maps in plugin/assets folder) -->
  <script src="admin/vendor/plugins/jvectormap/jquery.jvectormap.min.js"></script>
  <script src="admin/vendor/plugins/jvectormap/assets/jquery-jvectormap-us-lcc-en.js"></script>

  <!-- FullCalendar Plugin + moment Dependency -->
  <script src="admin/vendor/plugins/fullcalendar/lib/moment.min.js"></script>
  <script src="admin/vendor/plugins/fullcalendar/fullcalendar.min.js"></script>
  
  <!-- Magnific Popup Plugin -->
  <script src="admin/vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <!--<script src="admin/assets/js/utility/utility.js"></script>
  <script src="admin/assets/js/demo/demo.js"></script>
  <script src="admin/assets/js/main.js"></script>-->

  <script src="admin/assets/js/demo/demo.js"></script>
  <script src="admin/assets/js/main.js"></script>
  
  <!-- Select2 Plugin Plugin -->
  <script src="admin/vendor/plugins/select2/select2.min.js"></script>
  
  
  
  
  
  
  <!-- Time/Date Plugin Dependencies -->
  <script src="admin/vendor/plugins/globalize/globalize.min.js"></script>
  <script src="admin/vendor/plugins/moment/moment.min.js"></script>
  
  <!-- Bootstrap Maxlength plugin -->
  <script src="admin/vendor/plugins/maxlength/bootstrap-maxlength.min.js"></script>
  
  <!-- DateTime Plugin -->
  <script src="admin/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>
  
  <!-- MaskedInput Plugin -->
  <script src="admin/vendor/plugins/jquerymask/jquery.maskedinput.min.js"></script>
  
  
  
  
  <!-- FileUpload JS -->
  <script src="admin/vendor/plugins/fileupload/fileupload.js"></script>
  <script src="admin/vendor/plugins/holder/holder.min.js"></script>

  <!-- Tagmanager JS -->
  <script src="admin/vendor/plugins/tagsinput/tagsinput.min.js"></script>
  
  
  
  
  
  
  
  
  <!-- Datatables -->
  <script src="admin/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>
  
  <!-- Datatables Tabletools addon -->
  <script src="admin/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

  <!-- Datatables ColReorder addon -->
  <script src="admin/vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>

  <!-- Datatables Bootstrap Modifications  -->
  <script src="admin/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>


  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    //Core.init();

    // Init Demo JS  
    Demo.init();

    // select list dropdowns - placeholder like creation
    var selectList = $('.admin-form select');
    selectList.each(function(i, e) {
      $(e).on('change', function() {
        if ($(e).val() == "0") $(e).addClass("empty");
        else $(e).removeClass("empty")
      });
    });
    selectList.each(function(i, e) {
      $(e).change();
    });

    // Init TagsInput plugin
    $("input#tagsinput").tagsinput({
      tagClass: function(item) {
        return 'label bg-primary light';
      }
    });

  });
  </script>




  <style type="text/css">
       
        .file-upload {
            position: relative;
            overflow: hidden;
            margin: 10px; 
        }

        .file-upload input.file-input {
          position: absolute;
          top: 0;
          right: 0;
          margin: 0;
          padding: 0;
          font-size: 20px;
          cursor: pointer;
          opacity: 0;
          filter: alpha(opacity=0); 
        }


        
        
        
    </style>




	<script type="text/javascript">

		function explode(){
			if ($("#ocultoCarrusel").get(0).value == "0"){
				$("#ocultoCarrusel").get(0).value = "1";
				$("#item1-carrusel").fadeOut(300, function(){
					$("#item2-carrusel").fadeIn(300);
				});
				$("#item1-carrusel-small").fadeOut(300, function(){
					$("#item2-carrusel-small").fadeIn(300);
				});
			} else {
				$("#ocultoCarrusel").get(0).value = "0";
				$("#item2-carrusel").fadeOut(300, function(){
					$("#item1-carrusel").fadeIn(300);
				});
				$("#item2-carrusel-small").fadeOut(300, function(){
					$("#item1-carrusel-small").fadeIn(300);
				});
			}
		  setTimeout(explode, 10000);
		}
		setTimeout(explode, 10000);







		function explodedown(){
			if ($("#ocultoCarruselDown").get(0).value == "0"){
				$("#ocultoCarruselDown").get(0).value = "1";
				$("#item1-carrusel-down").fadeOut(300, function(){
					$("#item2-carrusel-down").fadeIn(300);
				});
				/*$("#item1-carrusel-small").fadeOut(300, function(){
					$("#item2-carrusel-small").fadeIn(300);
				});*/
			} else {
				$("#ocultoCarruselDown").get(0).value = "0";
				$("#item2-carrusel-down").fadeOut(300, function(){
					$("#item1-carrusel-down").fadeIn(300);
				});
				/*$("#item2-carrusel-small").fadeOut(300, function(){
					$("#item1-carrusel-small").fadeIn(300);
				});*/
			}
		  setTimeout(explodedown, 10000);
		}
		setTimeout(explodedown, 10000);

	</script>

	<script type="text/javascript">
	jQuery(document).ready(function() {


		







		$("#img_logo_nav").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_over_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  }
		});

		$("#img_logo_nav_text").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_over_img.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_img.png');
		  }
		});


		$("#img_loggin").on({
		 "mouseover" : function() {
		    this.src = 'images/icon_user_jj.png';
		  },
		  "mouseout" : function() {
		    this.src='images/icon_user.png';
		  }
		});




		$(window).on({
		 "scroll" : function() {
		    //$("#div-nav").height(50);
		    $("div-nav").css("height", 50);
		  }
		});

	});
	</script>


</body>
</html>	