<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Find My Bicy" />
	<meta name="description" content="Find My Bicy">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">


	<meta name="google-site-verification" content="3DTsueSjFNzNYSH0Ls9h_rozaHSoJiC51-bX2KIiycw" />





	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
	<style>
  		.carousel-inner > .item > img,
	  	.carousel-inner > .item > a > img {
	      width: 100%;
	      margin: auto;
	 	}

	 	.carousel-caption {
		    position: absolute;
		    top: 0;
		    left: 0;
		    right: 0;
		    display: -webkit-box;
		    display: -moz-box;
		    display: -ms-flexbox;
		    display: -webkit-flex;
		    display: flex;
		    -webkit-box-align: center;
		    -moz-box-align: center;
		    -ms-flex-align: center;
		    -webkit-align-items: center;
		    align-items: center;
		    
		}

		.carousel-caption-down {
		    position: relative;
		    top: 0;
		    left: 0;
		    right: 0;
		    display: -webkit-box;
		    display: -moz-box;
		    display: -ms-flexbox;
		    display: -webkit-flex;
		    display: flex;
		    -webkit-box-align: center;
		    -moz-box-align: center;
		    -ms-flex-align: center;
		    -webkit-align-items: center;
		    align-items: center;
		    
		}
	</style>




	<title>FindMyBicy - Rodar nunca fue tan seguro</title>

	<!-- Royal Preloader CSS -->
	<link rel="stylesheet" type="text/css" href="css/royal_preloader.css">

	<!-- jQuery Files -->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

	<!-- Royal Preloader -->
	<script type="text/javascript" src="js/royal_preloader.min.js"></script>

	<!-- Revolution Slider CSS -->
	<link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
	<link rel="stylesheet" type="text/css" href="revolution/css/layers.css">

	<!-- Revolution Slider Navigation CSS -->
	<link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">

	<!-- Stylesheets -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet" title="main-css">
	<link href="css/bootstrap-social.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet" >
	<link href="css/jquery.snippet.css" rel="stylesheet">
	<link href="css/buttons.css" rel="stylesheet">

	<link rel="alternate stylesheet" type="text/css" href="css/colors/silver.css" title="silver">
	<link rel="alternate stylesheet" type="text/css" href="css/width-full.css" title="width-full">
	<link rel="alternate stylesheet" type="text/css" href="css/width-boxed.css" title="width-boxed">

	<!-- Icon Fonts -->
	<link href='css/ionicons.min.css' rel='stylesheet' type='text/css'>
	<link href='css/font-awesome.css' rel='stylesheet' type='text/css'>

	<!-- Magnific Popup -->
	<link href='css/magnific-popup.css' rel='stylesheet' type='text/css'>


	<link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

</head>
<body class="royal_preloader scrollreveal">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    

	<div id="royal_preloader"></div>



	<!-- Begin Boxed or Fullwidth Layout -->
	<div id="bg-boxed">
	    <div class="boxed">

			<!-- Begin Header -->
			<header>

				<!-- Begin Top Bar -->
				<!--<div class="top-bar">
					<div class="container">
						<div class="row">
							<!-- Address and Phone - ->
							<div class="col-sm-7 hidden-xs">
							</div>
							<!-- Social Buttons - ->
							<div class="col-sm-5 text-right">
				        <ul class="topbar-list list-inline">
					        <!--<li>
										<a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-facebook"></i>
										</a>
									</li>-->
									<!--<li>
										<a class="btn btn-social-icon btn-rw btn-primary btn-xs">
											<i class="fa fa-google"></i>
										</a>
									</li>- ->
									<li><a data-toggle="modal" data-target="#login">Ingresa</a></li>
									<li><a href="registro.php">Reg&iacute;strate</a></li>
								</ul>
							</div>
						</div><!--/row - ->
					</div><!--/container header - ->
				</div>--><!--/top bar -->
				<!-- End Top Bar -->

				<!-- Login -->
				<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
					<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
				        <div class="modal-content" style="border-radius: 10px">
				            <!--<div class="modal-header" style="background: white; border-radius: 10px">
				                <button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				                <!--<h4 class="modal-title" id="loginLabel">Ingresa</h4>- ->
				                <!--<img src="images/logo_login.png" style="width: 135px;" />- ->
				            </div>-->
				            <!--<button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>-->
				            <div class="modal-body">
											<form role="form">
													<div class="form-group">
															<!--<label>Correo</label>-->
															<div class="input-group" style="width: 100%">
																	<!--<div class="input-group-addon"><span class="ion-android-mail" style="font-size:9px;"></span></div>-->
																	<input class="form-control" type="email" id="email" placeholder="Ingresa tu correo" style="width: 100%">
															</div>
													</div>
													<div class="form-group">
														<!--<label>Contraseña</label>-->
														<div class="input-group" style="width: 100%">
																	<!--<div class="input-group-addon"><span class="ion-ios7-locked"></span></div>-->
																<input type="password" class="form-control" id="password" placeholder="Ingresa tu contraseña">
															</div>
													</div>
													<!--<div class="checkbox">
															<label>
																<input type="checkbox"> Recu&eacute;rdame
															</label>
													</div>-->

													<div class="row" style="padding-left: 20px; padding-right: 20px; padding-top: 5px; padding-bottom: 5px">
														<span>¿No tienes cuenta? </span><a>Reg&iacute;strate</a>
													</div>

													<!--<hr class="mb20 mt15">
													<button type="submit" class="btn btn-rw btn-primary">Submit</button> &nbsp;&nbsp;&nbsp;<small><a href="#">Forgot your password?</a></small>-->
											</form><!-- /form -->
				            </div>
				            <div class="modal-footer">
				                <button type="button" class="btn btn-rw btn-primary" style="width: 100%"><b>INGRESAR</b></button>
				            </div>
				        </div><!-- /modal content -->
				    </div><!-- /modal dialog -->
				</div><!-- /modal holder -->
				<!-- End Login -->





		     	





				<!-- Begin Navigation -->
		     	<!--<div class="navbar-wrapper" style="width: 100%; position:relative; z-index:100;" id="div-nav">-->
		     	<div class="navbar-wrapper" style="width: 100%; height: 40px; position:fixed; left:0px; right:0px; top:0px; z-index:100;" id="div-nav">
					<div class="navbar navbar-main" id="fixed-navbar" style="background: rgba(0, 0, 0, 1.0); height: 40px;">

						<div class="container" >
							<div class="row">
								<div class="col-sm-12 column-header">
									<div class="navbar-header">
										<!-- Brand -->
										<a href="index.php" ><!--class="navbar-brand"-->
											<img src="images/logo_img.png" style="height: 40px; margin-top: 5px; margin-right: 20px; margin-bottom: 5px;" id="img_logo_nav" />
											<!--<img src="images/logo_text.png" style="width: 100px" id="img_logo_nav_text" style="margin-top: 30px;" />-->
										</a>
										<!-- Mobile Navigation -->
										<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
								            <span class="sr-only">Toggle navigation</span>
								            <span class="icon-bar"></span>
								            <span class="icon-bar"></span>
								            <span class="icon-bar"></span>
										</button>
									</div><!-- /navbar header -->

									<!-- Main Navigation - Explained in Documentation -->
									<nav class="navbar-collapse collapse navHeaderCollapse" role="navigation">
										<ul class="nav navbar-nav navbar-right" style="margin-top: -4px;">
											<!--<li>
									      <a href="index.php">Inicio</a>
									    </li>-->
											<!--<li>
									      <a href="servicios.html">Servicios</a>
									    </li>-->
											<!--<li>
									      <a href="#">Find My Bicy</a>
									    </li>-->
									    <li>
									    	<a style="border-style: solid; border-width: 2px; border-color: #53c5ea; color: white; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; font-weight: bold; margin-top: 15px">Find My Bicy</a>
									    </li>
										<li>
									      <a style="color: white; margin-top: -7px;" href="contacto.php">Contacto</a>
									    </li>
									    <li>
									      <a data-toggle="modal" data-target="#login" style="margin-top: -12px;"><img id="img_loggin" src="images/icon_user.png" style="width: 25px;" /></a>
									    </li>
											<!--<li class="dropdown hidden-sm hidden-xs">
						                    	<a href="#" class="ml10 nav-circle-li dropdown-toggle dropdown-form-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i></a>
						                        <ul class="fadeInUp-animated dropdown-menu dropdown-menu-user cart">
						                            <li id="dropdownForm">
						                            	<div class="dropdown-form">
																							<table class="table table-hover no-margin">
																								<thead>
																									<tr>
																										<th class="quantity">Cantidad</th>
																										<th class="product">Producto</th>
																										<th class="amount">Subtotal</th>
																									</tr>
																								</thead>
																								<tbody>
																									<tr>
																										<td class="quantity">2 x</td>
																										<td class="product"><a href="shop-product.html">Hunter</a><span class="small">32GB 10.0 Megapixel</span></td>
																										<td class="amount">$700.00</td>
																									</tr>
																									<tr>
																										<td class="quantity">3 x</td>
																										<td class="product"><a href="shop-product.html">Hunter</a><span class="small">Quad Core 5GB</span></td>
																										<td class="amount">$500.00</td>
																									</tr>
																									<tr>
																										<td class="quantity">3 x</td>
																										<td class="product"><a href="shop-product.html">Hunter</a><span class="small">1 Foot Tall - 2FT Wingspan</span></td>
																										<td class="amount">$1500.00</td>
																									</tr>
																									<tr>
																										<td class="total-quantity" colspan="2">Total 8 Productos</td>
																										<td class="total-amount">$3000.00</td>
																									</tr>
																									<tr>
																										<td class="text-right" colspan="3">
																											<a href="shop-cart.php"><button class="btn btn-rw btn-primary btn-sm">Ver Carrito</button></a>
																											<a href="shop-checkout-1.html"><button class="btn btn-rw btn-primary btn-sm">Realizar Compra</button></a>
																										</td>
																									</tr>
																								</tbody>
																							</table>
						                                </div><!-- /dropdown form - ->
						                            </li><!-- /dropdownForm list item - ->
						                        </ul><!-- /cart dropdown - ->
						                	</li>--><!-- /cart navbar list item -->
										</ul><!-- /navbar right -->
									</nav><!-- /nav -->
								</div>
							</div>
						</div><!-- /container header -->
					</div><!-- /navbar -->
				</div>--><!-- /navbar wrapper -->
				<!-- End Navigation -->









				















			</header><!-- /header -->
			<!-- End Header -->










			<!--<section class="row" style="padding-top:10px; padding-right: 50px; background: black; height: 60px;">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
				</button>
				<nav class="navbar" role="navigation">
					<ul class="nav navbar-nav navbar-left">
						<li>
							<img src="images/logo_img.png" style="height: 40px; margin-left:100px; margin-top: 5px; margin-right: 20px; margin-bottom: 5px;" id="img_logo_nav" />
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right" >
					    <li>
					    	<a href="#" style="border-style: solid; border-width: 2px; border-color: #53c5ea; color: white; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; font-weight: bold; ">Find My Bicy</a>
					    </li>
						<li>
					      <a style="color: white; margin-top: -7px;" href="contacto.php">Contacto</a>
					    </li>
					    <li>
					      <a data-toggle="modal" data-target="#login" style="margin-top: -12px;"><img id="img_loggin" src="images/icon_user.png" style="width: 25px;" /></a>
					    </li>
					</ul>
				</nav>
			</section>


			<section class="row" style="padding-top:10px; padding-right: 50px; background: black; height: 60px;">
				<div class="col-md-3">
					<img src="images/logo_img.png" style="height: 40px; margin-left:100px; margin-top: 5px; margin-right: 20px; margin-bottom: 5px;" id="img_logo_nav" />
				</div>
				<div class="col-md-9">
					<ul class="nav navbar-nav navbar-right" >
					    <li>
					    	<a style="border-style: solid; border-width: 2px; border-color: #53c5ea; color: white; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; font-weight: bold; ">Find My Bicy</a>
					    </li>
						<li>
					      <a style="color: white; margin-top: -7px;" href="contacto.php">Contacto</a>
					    </li>
					    <li>
					      <a data-toggle="modal" data-target="#login" style="margin-top: -12px;"><img id="img_loggin" src="images/icon_user.png" style="width: 25px;" /></a>
					    </li>
					</ul>
				</div>
			</section>-->















			<div id="carrusel" class="hidden-xs" style="height: 600px; width: 100%;">
			  	<div id="item1-carrusel">
		      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 600px; width: 100%;">
		    	</div>
			    <div id="item2-carrusel" style="height: 600px; width: 100%; display: none; overflow: hidden;">
					<video loop="loop" style="width: 100%;" autoplay="autoplay">
				        <source src="videos/hunter.mp4" type="video/mp4"/>
				    </video>
			    </div>

			    <div class="carousel-caption" style="color: white; background: transparent; margin-top: -200px; font-size: 30px; font-family: Verdana; text-align: center;">
	        		<p style="color: white;  width: 100%; "><b>Rodar nunca fue tan seguro</b></p>
	      		</div>
	      		<div class="carousel-caption" style="color: white; background: transparent; margin-top: -100px; font-size: 15px; font-family: Verdana;">
	        		<p style="color: white; width: 100%;">Para <strong>todas las bicicletas</strong></p>
	      		</div>
	      		<div class="carousel-caption" style="color: white; background: transparent; margin-top: 0px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
	        		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 18px; font-family: Arial, Gadget, sans-serif; "><b>Find</b> My Bicy &nbsp;<span class="glyphicon glyphicon-chevron-down"></span> </a>
	      		</div>
			</div>


			<div id="carrusel-small" class="hidden-md hidden-lg hidden-sm" style="height: 300px; width: 100%;">
			  	<div id="item1-carrusel-small">
		      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 300px; width: 100%;">
		    	</div>
			    <div id="item2-carrusel-small" style="height: 300px; width: 100%; display: none; overflow: hidden;">
					<video loop="loop" style="width: 100%;" autoplay="autoplay">
				        <source src="videos/hunter.mp4" type="video/mp4"/>
				    </video>
			    </div>

			    <div class="carousel-caption" style="color: white; background: transparent; margin-top: -400px; font-size: 20px; font-family: Verdana; text-align: center;">
	        		<p style="color: white;  width: 100%; "><b>Rodar nunca fue tan seguro</b></p>
	      		</div>
	      		<div class="carousel-caption" style="color: white; background: transparent; margin-top: -300px; font-size: 12px; font-family: Verdana;">
	        		<p style="color: white; width: 100%;">Para <strong>todas las bicicletas</strong></p>
	      		</div>
	      		<div class="carousel-caption" style="color: white; background: transparent; margin-top: -200px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
	        		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; "><b>Find</b> My Bicy  <span class="glyphicon glyphicon-chevron-down"></span></a>
	      		</div>
			</div>






			<!--<div>
				<div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 600px; width: 100%">
				  <!-- Indicators - ->
				  <!--<ol class="carousel-indicators">
				    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#myCarousel" data-slide-to="1"></li>
				  </ol>- ->

				  <!-- Wrapper for slides - ->
				  <div class="carousel-inner" role="listbox">
				    
				    <div class="item active" style="height: 600px; width: 100%">
				      <img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 600px">

				      <div class="carousel-caption" style="color: white; background: transparent; margin-bottom: 300px">
				        <span style="color: white; font-size: 40px; width: 100%">Rodar nunca fue tan seguro</span><b />
				        <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
				      </div>

				    </div>

				    <div class="item" >
				    	<div style="text-align: center; justify-content: center; align-content: center; flex-direction: column;">
				    		<video width="100%" height="100%" autoplay loop>
							  	<source src="videos/hunter.mp4" type="video/mp4">
								Your browser does not support the video tag.
							</video>
				    	</div>
				    </div>

				    

				  </div>

				  <!-- Left and right controls -->
				  <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>- ->
				</div>
			</div>-->









			<!-- Start Slider Revolution -->
			<!--<div class="rev_slider_wrapper">
				<div class="rev_slider" data-version="5.0" id="slider1">
					<ul>

						<!-- Slide 1 - ->
						<li data-delay="10000"
						data-description="Raleway Bootstrap Template"
						data-transition="zoomin"
						data-easein="default"
						data-easeout="default"
						data-masterspeed="default"
						data-param1="test"
						data-rotate="0"
						data-thumb="revolution/assets/images/hunter.jpg"
						data-slotamount="default"
						data-title="Find My Bicy">

							<div class="rs-background-video-layer"
							data-forcerewind="on"
							data-volume="mute"
							data-forcerewind="on"
							data-volume="mute"
							data-videowidth="100%"
							data-videoheight="100%"
							data-videomp4="videos/hunter.mp4"
							data-videopreload="preload"
							data-videoloop="loopandnoslidestop"
							data-forceCover="1"
							data-aspectratio="16:9"
							data-autoplay="true"
							data-autoplayonlyfirsttime="false"
							data-nextslideatend="true"
							></div>

							<div class="tp-caption NotGeneric-Title tp-resizeme rs-parallaxlevel-0"
							data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
							data-fontsize="['30','30','30','25']"
							data-lineheight="['70','70','70','50']"
							data-whitespace="nowrap"
							data-transform_idle="o:1;"
							data-transform_in="z:0;rX:0;rY:0;rZ:5deg;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Back.easeOut;"
							data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="1000"
							style="z-index: 6; white-space: nowrap;">Rodar nunca fue tan seguro!
							</div>

							<div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-0"
							data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']"
							data-transform_idle="o:1;"
							data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
							data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
							data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="1500"
							style="z-index: 7; white-space: nowrap;">Para <strong>todas las bicicletas</strong>
							</div>

							<div class="tp-caption"
							data-x="500"
							data-y="105"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="y:10px;opacity:0;s:700;e:Power3.easeOut;"
							data-transform_out="y:10px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:15px;font-family:'Raleway' sans-serif;font-weight:200;  white-space: nowrap;">
							Para <strong>todas las bicicletas</strong>.
							</div>

							<!--<div class="tp-caption NotGeneric-Icon tp-resizeme rs-parallaxlevel-0"
							data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
							data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']"
							data-transform_idle="o:1;"
							data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;"
							data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
							data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
							data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
							data-start="2000"
							style="z-index: 8;font-size:50px;line-height:52px; white-space: nowrap;"><i class="fa fa-bicycle"></i>
							</div>-->

						</li>

						<!-- Slide 2 - ->
						<li data-delay="10000"
						data-description="Some Description"
						data-easein="default"
						data-easeout="default"
						data-masterspeed="default"
						data-param1="test"
						data-rotate="0"
						data-slotamount="default"
						data-thumb="images/backgrounds/hunter.jpg"
						data-title="Descripción"
						data-transition="slideremoveleft">

							<!-- MAIN IMAGE - ->
							<img alt=""
							data-bgfit="cover"
							data-bgposition="center center"
							data-bgrepeat="no-repeat"
							data-no-retina=""
							height="800"
							src="images/backgrounds/hunter.jpg"
							style="background-color:#000;"
							width="1732">

							<div class="tp-caption"
							data-x="500"
							data-y="105"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="y:10px;opacity:0;s:700;e:Power3.easeOut;"
							data-transform_out="y:10px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:35px;font-family:'Raleway' sans-serif;font-weight:200;border:2px solid #fff;padding:0 17px;">
							Tu bicicleta segura, en solo <strong>3 pasos</strong>.
							</div>

							<div class="tp-caption background-mainsub"
							data-x="500"
							data-y="195"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:100px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:100px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:27px;font-family:'FontAwesome' sans-serif;line-height:50px;text-align:center;padding-left:12px;padding-right:12px;border-radius:50%">

								<span class="fa fa-check"></span>

							</div>

							<div class="tp-caption"
							data-x="560"
							data-y="200"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:130px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:130px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:24px;font-family:'Open Sans' sans-serif;font-weight:300;">
							Crea tu cuenta de usuario.
							</div>

							<div class="tp-caption background-mainsub"
							data-x="500"
							data-y="260"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:160px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:160px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:27px;font-family:'FontAwesome' sans-serif;line-height:50px;text-align:center;padding-left:12px;padding-right:12px;border-radius:50%">

								<span class="fa fa-check"></span>

							</div>

							<div class="tp-caption"
							data-x="560"
							data-y="265"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:190px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:190px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:24px;font-family:'Open Sans' sans-serif;font-weight:300;">
							Adquiere tu dispositivo <strong>Hunter</strong> en nuestra tienda o en puntos autorizados.
							</div>

							<div class="tp-caption background-mainsub"
							data-x="500"
							data-y="324"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:220px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:220px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:27px;font-family:'FontAwesome' sans-serif;line-height:50px;text-align:center;padding-left:12px;padding-right:12px;border-radius:50%">

								<span class="fa fa-check"></span>

							</div>

							<div class="tp-caption"
							data-x="560"
							data-y="329"
							data-speed="600"
							data-start="1200"
							data-end="9400"
							data-endspeed="600"
							data-transform_idle="o:1;"
							data-transform_in="x:260px;opacity:0;s:1200;e:Power3.easeOut;"
							data-transform_out="y:260px;opacity:0;s:500;e:Power2.easeInOut;"
							style="color:#fff;font-size:24px;font-family:'Open Sans' sans-serif;font-weight:300;">
							Descarga nuestra aplicaci&oacute;n.
							</div>

						</li>

					</ul>
				</div>
			</div>--><!-- /rev slider -->
			<!-- End Slider Revolution -->








			<!-- Begin Menu -->
			<div class="pt40 pb40 border-bottom">
				<div class="container" style="padding-bottom: 30px;">
					<br>
					<p class="lead text-center" style="font-size: 14px; color: darkgray;">ELLOS CONFIAN EN NOSOTROS</p>
					<hr style="width:400px">
					<div class="row">
						<div class="col-sm-6 fadeInUp-animated text-center">
							<a href="http://www.ean.edu.co/"><img src="images/logo-ean.png" style="height: 80px;" /></span></a>
						</div>
						<div class="col-sm-6 fadeInUp-animated text-center">
							<a href="https://www.policia.gov.co/"><img src="images/logo_policia_nacional.png" style="height: 80px;" /></span></a>
						</div>
					</div>
				</div><!-- /container -->
			</div><!-- /cta -->
			<!-- End Menu -->










			<!-- Begin Menu -->
			<!--<div class="pt40 pb40 border-bottom">
				<div class="container">
					<br>
					<p class="lead text-center">Menu <b><mark>Find My Bicy</mark></b></p>
					<hr style="width:400px">
					<div class="row">
						<!-- Content 1 - ->
						<div class="col-sm-4 fadeInUp-animated text-center">
						    <a href="registro.php"><span class="ion-person bordered-icon-lg bordered-icon-color"></span></a>
						    <h4 class="mt15">Reg&iacute;strate.</h4>
						</div>
						<!-- Content 2 - ->
						<div class="col-sm-4 fadeInUp-animated text-center mt20-xs">
						    <a href="tienda.php"><span class="ion-bag bordered-icon-lg bordered-icon-color"></span></a>
						    <h4 class="mt15">Tienda.</h4>
						</div>
						<!-- Content 3 -->
						<!--<div class="col-sm-3 fadeInUp-animated text-center">
						    <a href="servicios.html"><span class="ion-grid bordered-icon-lg bordered-icon-color"></span></a>
						    <h4 class="mt15">Servicios.</h4>
						</div>-->
						<!-- Content 4 - ->
						<div class="col-sm-4 fadeInUp-animated text-center mt20-xs">
						    <a href="contacto.php"><span class="ion-email bordered-icon-lg bordered-icon-color"></span></a>
						    <h4 class="mt15">Contacto.</h4>
						</div>
					</div>
				</div><!-- /container - ->
			</div>--><!-- /cta -->
			<!-- End Menu -->

			<!-- Begin Content Section -->
			<section class="background-light-grey border-top" style="padding-bottom: 80px; padding-top: 60px;">
				<div class="container">
					<br>
					<div class="row mt40 mb40">

						<!-- Begin Carousel -->
						<div class="col-sm-6">
							<div id="aboutCarousel" class="carousel carousel-fade slide" data-ride="carousel">
								<!-- Indicators -->
								<ol class="carousel-indicators">
									<li data-target="#aboutCarousel" data-slide-to="0" class="active"></li>
									<li data-target="#aboutCarousel" data-slide-to="1"></li>
									<li data-target="#aboutCarousel" data-slide-to="2"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<div style="background-image:url('images/backgrounds/stock1.jpg'); height:255px;" data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"></div>
									</div>
									<div class="item">
										<div style="background-image:url('images/backgrounds/stock2.jpg'); height:255px" data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"></div>
									</div>
									<div class="item">
										<div style="background-image:url('images/backgrounds/stock3.jpg'); height:255px" data-0="background-position: 50% 0px;" data-500="background-position: 50% -70px;"></div>
									</div>
								</div>

								<!-- Controls -->
								<a class="left carousel-control" href="#aboutCarousel" data-slide="prev">
									<span class="ion-ios7-arrow-left carousel-arrow-left no-lineheight"></span>
								</a>
								<a class="right carousel-control" href="#aboutCarousel" data-slide="next">
									<span class="ion-ios7-arrow-right carousel-arrow-right no-lineheight"></span>
								</a>
							</div><!-- /carousel -->
						</div><!-- /column -->
						<!-- End Carousel -->

						<!-- Content 1 -->
						<div class="col-sm-6 mt30-xs" style="padding-left: 100px;">
							<div class="heading"><h4>¿Por qu&eacute; Find My Bicy?</h4></div>
							<p>Somos una empresa de origen colombiano legalmente constituida y autorizada, que ofrece seguridad a los BicyUsuarios mediante el uso de herramientas tecnológicas.</p>
		
						</div><!-- /column -->
						<!-- End Content 1 -->
					</div><!-- /row-->
				</div><!-- /container -->
			</section><!-- /section -->
			<!-- End Content Section -->















			<!-- Begin Content Section -->
			<section class="background-white border-top" style="padding-bottom: 80px; padding-top: 60px;">
				<div class="container">
					<br>
					<div class="row mt40 mb40">

						<!-- Begin Carousel -->
						<div class="col-sm-6">
							
							<h4 style="color: #000">Somos</h4><h1 style="color: #000">&nbsp;Seguridad</h1>
							</br>
							<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilizamos tecnología para ofrecer el nivel más alto de seguridad a los usuarios, mediante nuestra plataforma en página web y aplicativo móvil, donde el usuario podrá acceder a diferentes servicios, en cualquier momento, para mantener georeferenciada y vigilada su bicicleta</p>
							</br>
							</br>
							<div class="row">
								<div class="col-md-4" style="text-align: center; color: #53c5ea;">
									<img style="width:100px; " src="images/icono_servicio_localizacion.png" />
									</br>
									</br>
									<p>Localización bicicleta</p>
								</div>
								<div class="col-md-4" style="text-align: center; color: #53c5ea;">
									<img style="width:100px; " src="images/icono_servicio_parqueo.png" />
									</br>
									</br>
									<p>Modo parqueo por vibración o salida de zona</p>
								</div>
								<div class="col-md-4" style="text-align: center; color: #53c5ea;">
									<img style="width:100px; " src="images/icono_servicio_alerta.png" />
									</br>
									</br>
									<p>Alerta a Policía Nacional, Fiscalía General de la Nación, familia y amigos</p>
								</div>
							</div>

						</div><!-- /column -->
						<!-- End Carousel -->

						<!-- Content 1 -->
						<div class="col-sm-6 mt30-xs" style="padding-left: 100px;">
							
							<img src="images/img_bicicletas.jpg" style="width: 120%" />
		
						</div><!-- /column -->
						<!-- End Content 1 -->
					</div><!-- /row-->
				</div><!-- /container -->
			</section><!-- /section -->
			<!-- End Content Section -->












			<!--<section class="row">
				<div class="col-md-6" style="padding-top: 100px; padding-left: 100px; padding-right: 30px; padding-bottom: 30px;">
					<h4 style="color: #53c5ea">Somos</h4><h1 style="color: #53c5ea">&nbsp;Seguridad</h1>
					</br>
					<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilizamos tecnología para ofrecer el nivel más alto de seguridad a los usuarios, mediante nuestra plataforma en página web y aplicativo móvil, donde el usuario podrá acceder a diferentes servicios, en cualquier momento, para mantener georeferenciada y vigilada su bicicleta</p>
					
					
				</div>
				<div class="col-md-6" style="padding:100px;">
					<img src="images/img_bicicletas.jpg" style="width: 70%" />
				</div>
			</section>-->













			<!-- Begin Content Section -->
			<section class="background-light-grey border-top" style="padding-bottom: 80px; padding-top: 80px;">
				<div class="container">
					<div class="row" style="text-align: center">
						<h1 style="color: #53c5ea">¿Cómo empezar?</h1>
						</br>
						<h5>Disponible para <b>todas las bicicletas</b></h5>
					</div>
					<br>
					<div class="row mt40 mb40">

						<!-- Begin Carousel -->
						<div class="col-sm-4" style="text-align: center;">
							<img src="images/logomybicy.png" style="height: 200px;" />
							</br>
							</br>
							</br>
							</br>
							<h4>Paso 1: <a>Adquirir GPS</a> y pagar servicio.</h4>
							</br>
							</br>
							</br>
							<h6 style="font-size: 11px">El costo periodico se deriva del operador del servicio</h6>

						</div><!-- /column -->
						<!-- End Carousel -->

						<!-- Content 1 -->
						<div class="col-sm-4 mt30-xs" style="text-align: center;">
							<video loop="loop" style="height: 198px;" autoplay="autoplay">
						        <source src="videos/hunter.mp4" type="video/mp4"/>
						    </video>
							</br>
							</br>
							</br>
							</br>
							<h4>Paso 2: Completar registro e instalar GPS en la bicicleta.</h4>
							</br>
							</br>
							<h6 style="font-size: 11px">Ver video instructivo en cuenta de usuario</h6>
		
						</div><!-- /column -->

						<div class="col-sm-4 mt30-xs" style="text-align: center;">
							<img src="images/logofindmybicy.png" style="height: 200px;" />
							</br>
							</br>
							</br>
							</br>
							<h4>Paso 3: <a>Accede en plataforma web</a> o descarga nuestro aplicativo móvil.</h4>
							</br>
							</br>
							<div class="row" style="text-align: center">
								<img src="images/logo-appstore.png" style="width: 120px;" />
								<img src="images/logo-googleplay.png" style="width: 120px;" />
							</div>
							
							
		
						</div><!-- /column -->

						<!-- End Content 1 -->
					</div><!-- /row-->
				</div><!-- /container -->
			</section><!-- /section -->
			<!-- End Content Section -->










			<!--<section class="row background-white border-top">
				<div class="row" style="text-align: center">
					<h1 style="color: #53c5ea">¿Cómo empezar?</h1>
					</br>
					<h5>Disponible para <b>todas las bicicletas</b></h5>
				</div>
				</br>
				</br>
				</br>
				</br>
				<div class="col-md-4" style="text-align: center;">
					<h3>Paso 1:</h3>
					</br>
					<p><a>Adquirir GPS</a> y pagar servicio.</p>
				</div>
				<div class="col-md-4" style="text-align: center;">
					<h3>Paso 2:</h3>
					</br>
					<p>Completar registro e instalar GPS en la bicicleta.</p>
					</br>
					<h6>* Ver video instructivo en cuenta de usuario</h6>
				</div>
				<div class="col-md-4" style="text-align: center;">
					<h3>Paso 3:</h3>
					</br>
					<p>Accede en plataforma web o descarga nuestro aplicativo móvil.</p>
					</br>
					<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 18px; font-family: Arial, Gadget, sans-serif; "><b>Find</b> My Bicy &nbsp;<span class="glyphicon glyphicon-chevron-down"></span> </a>
					</br>
					</br>
					<div class="row" style="text-align: center">
						<img src="images/logo-appstore.png" style="width: 120px;" />
						<img src="images/logo-googleplay.png" style="width: 120px;" />
					</div>
					</br>
					</br>
					</br>
				</div>
				</br>
				</br>
				</br>
				</br>
				</br>
				</br>
			</section>-->












			<section class="row" style="padding-top: 80px; padding-bottom: 60px;">

				<div class="row" style="text-align: center">
					<h1 style="color: #53c5ea">Testimonios</h1>
					</br>
					<h5>Nuestra prioridad es prestar un servicio de alta seguridad para los BicyUsuarios</h5>
					</br>
					</br>
				</div>
				<div class="row">
					<div class="col-md-6" style="text-align: center;">
						<img src="images/carlos_vives.jpg" style="width: 80%" />
						</br>
						</br>
						</br>
						</br>
					</div>
					<div class="col-md-6" style="padding-top: 100px; padding-left: 30px; padding-right: 150px; padding-bottom: 30px;">
						<p><i>"Gracias a Find My Bicy rodar es más seguro y no he vuelto a perder mi bicicleta. Es un dispositivo tecnológico que ofrece un servicio de alta calidad y cobertura."</i></p>
						</br>
						<h3>Carlos Vives</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6" style="text-align: center;">
						<img src="images/enrique_penhalosa.jpg" style="width: 50%" />
						</br>
						</br>
						</br>
						</br>
					</div>
					<div class="col-md-6" style="padding-top: 100px; padding-left: 30px; padding-right: 150px; padding-bottom: 30px;">
						<p><i>"Find My Bicy mejora la seguridad de los BicyUsuarios en la capital colombiana, incluyéndome. Además es una herramienta que ayuda a la administración municipal, con el control de la delincuencia"</i></p>
						</br>
						<h3>Enrique Peñalosa</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6" style="text-align: center;">
						<img src="images/mariana_pajon.jpg" style="width: 50%" />
						</br>
						</br>
						</br>
						</br>
					</div>
					<div class="col-md-6" style="padding-top: 100px; padding-left: 30px; padding-right: 150px; padding-bottom: 30px;">
						<p><i>"Me gusta Find My Bicy porque me ayuda a mantener vigilada mi bicicleta a toda hora."</i></p>
						</br>
						<h3>Mariana Pajón</h3>
					</div>
				</div>
				</br>
				<div class="row" style="text-align: center">
					<h4><a>Ver Mas</a></h4>
					</br>
				</div>
			</section>

















			<div id="carrusel-down" class="row hidden-xs" style="height: 600px; width: 100%; margin-left: 0px">
			  	<div id="item1-carrusel-down">
		      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 600px; width: 100%;">
		    	</div>
			    <div id="item2-carrusel-down" style="height: 600px; width: 100%; display: none; overflow: hidden;">
					<video loop="loop" style="width: 100%;" autoplay="autoplay">
				        <source src="videos/hunter.mp4" type="video/mp4"/>
				    </video>
			    </div>

			    <div style="color: white; background: transparent; margin-top: -300px; font-size: 35px; font-family: Verdana; text-align: center;">
	        		<p style="color: white;  width: 100%; ">Vigilemos juntos tu bicicleta</p>
	      		</div>
	      		<div class="carousel-caption-down" style="color: white; background: transparent; margin-top: 0px; font-size: 15px; font-family: Verdana; text-align: center; margin-bottom: 10px;">
	        		<p style="color: white; width: 100%;">Rodar nunca fue tan seguro</p>
	      		</div>
	      		<div class="carousel-caption-down" style="color: white; background: transparent; margin-top: 0px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
	        		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 18px; font-family: Arial, Gadget, sans-serif; "><b>Find</b> My Bicy &nbsp;<span class="glyphicon glyphicon-chevron-down"></span> </a>
	      		</div>
			</div>


			<div id="carrusel-small-down" class="hidden-md hidden-lg hidden-sm" style="height: 300px; width: 100%;">
			  	<div id="item1-carrusel-small-down">
		      		<img src="images/backgrounds/hunter.jpg" alt="Chania" style="height: 300px; width: 100%;">
		    	</div>
			    <div id="item2-carrusel-small-down" style="height: 300px; width: 100%; display: none; overflow: hidden;">
					<video loop="loop" style="width: 100%;" autoplay="autoplay">
				        <source src="videos/hunter.mp4" type="video/mp4"/>
				    </video>
			    </div>

			    <div class="carousel-caption-down" style="color: white; background: transparent; margin-top: -400px; font-size: 20px; font-family: Verdana; text-align: center;">
	        		<p style="color: white;  width: 100%; ">Vigilemos juntos tu bicicleta</p>
	      		</div>
	      		<div class="carousel-caption-down" style="color: white; background: transparent; margin-top: -300px; font-size: 12px; font-family: Verdana;">
	        		<p style="color: white; width: 100%;">Rodar nunca fue tan seguro</p>
	      		</div>
	      		<div class="carousel-caption-down" style="color: white; background: transparent; margin-top: -200px; text-align: center; justify-content: center; align-content: center; flex-direction: column;">
	        		<a style="border-style: solid; border-width: 3px; border-color: #53c5ea; color: white; background: #53c5ea; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; "><b>Find</b> My Bicy &nbsp;<span class="glyphicon glyphicon-chevron-down"></span></a>
	      		</div>
			</div>




			











			<!-- Begin Footer -->
			<footer class="footer" style="padding-top: 80px;">
				<div class="container">
					<div class="row">

						<!-- Social -->
						<!--<div class="col-sm-4 mg25-xs">
							<!--<div class="heading-footer"><h4>Redes Sociales</h4></div>- ->
								<span href="shop-single-product.html" class="fa fa-facebook bordered-icon-dark bordered-icon-sm mb5"></span>
								<span class="fa fa-instagram bordered-icon-dark bordered-icon-sm mb5 mt10"></span>
								<span class="fa fa-youtube bordered-icon-dark bordered-icon-sm mb5"></span>
								</br>
								</br>
								<div class="row" style="text-align: left">
									<img src="images/logo-appstore.png" style="width: 120px;" />
									<img src="images/logo-googleplay.png" style="width: 120px;" />
								</div>
						</div>-->

						<!-- Contact -->
						<div class="col-sm-6 mg25-xs">
							<div class="heading-footer"><h4 style="color: #53c5ea">Find My Bicy</h4></div>
							<p><span style="color: white" class="glyphicon glyphicon-home"></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;¿Quiénes somos?</a></small></p>
							<p><span style="color: white" class="glyphicon glyphicon-bullhorn"></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;TESTIMONIOS</a></small></p>
							<p><span style="color: white" class="glyphicon glyphicon-shopping-cart"></span><small class="address">&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: white;" href="">Puntos de venta</a></small></p>
							<p><span style="color: white" class="glyphicon glyphicon-earphone"></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;Contacto</a></small></p>
							</br>
							<p></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;Términos y condiciones</a></small></p>
							<p></span><small class="address"><a style="color: white;" href="">&nbsp;&nbsp;&nbsp;&nbsp;Política de privacidad</a></small></p>
							<!--<p><span class="ion-home footer-info-icons"></span><small class="address">Calle 00 # 00-00 Bogot&aacute;</small></p>
							<p><span class="ion-email footer-info-icons"></span><small class="address"><a href="mailto:info@findmybicy.com">info@findmybicy.com</a></small></p>
							<p><span class="ion-ios7-telephone footer-info-icons"></span><small class="address">+573156607087</small></p>-->
						</div>


						<div class="col-sm-6 mg25-xs">
							
							<a style="border-style: solid; border-width: 2px; border-color: #53c5ea; color: white; border-radius: 5px; padding-top: 5px; padding-bottom: 5px; padding-left: 15px; padding-right: 15px; text-decoration: none; font-size: 15px; font-family: Arial, Gadget, sans-serif; font-weight: bold; margin-top: 15px">Find My Bicy</a>
							</br>
							</br>
							</br>
							<span href="shop-single-product.html" class="fa fa-facebook bordered-icon-dark bordered-icon-sm mb5"></span>
								<span class="fa fa-instagram bordered-icon-dark bordered-icon-sm mb5 mt10"></span>
								<span class="fa fa-youtube bordered-icon-dark bordered-icon-sm mb5"></span>
								</br>
								</br>
								</br>
								<div class="row" style="text-align: left; margin-left: 2px;">
									<img src="images/logo-appstore.png" style="width: 120px;" />
									<img src="images/logo-googleplay.png" style="width: 120px;" />
								</div>

						</div>

					</div><!-- /row -->

					<!-- Copyright -->
					<div class="row">
						<hr class="dark-hr">
						<div class="col-sm-11 col-xs-10">
							<p class="copyright">© 2017 Find My Bicy. Todos los derechos reservados.</b></p>
						</div>
						<div class="col-sm-1 col-xs-2 text-right">
							<a href="#" class="scroll-top"><div class="footer-scrolltop-holder"><span class="ion-ios7-arrow-up footer-scrolltop"></span></div></a>
						</div>
					</div><!-- /row -->
				</div><!-- /container -->
			</footer><!-- /footer -->
			<!-- End Footer -->

		</div><!-- /boxed -->
	</div><!-- /bg boxed-->
	<!-- End Boxed or Fullwidth Layout -->

	<input type="hidden" id="ocultoCarrusel" value="0" />

	<!-- Javascript Files -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="js/scrollReveal.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>
	<script type="text/javascript" src="js/style-switcher.js"></script><!-- Remove for production -->
	<script type="text/javascript" src="js/activate-snippet.js"></script>
	<script type="text/javascript" src="js/skrollr.min.js"></script>

	<!-- On Scroll Animations - scrollReveal.js -->
    <script>
		var config = {
		easing: 'hustle',
		mobile:  true,
		delay:  'onload'
		}
		window.sr = new scrollReveal( config );
    </script>

	<!-- Slider Revolution JS -->
	<script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>

	<!-- Slider Revolution Extensions (Load Extensions only on Local File Systems The following part can be removed on Server for On Demand Loading) -->
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<script type="text/javascript">

		function explode(){
			if ($("#ocultoCarrusel").get(0).value == "0"){
				$("#ocultoCarrusel").get(0).value = "1";
				$("#item1-carrusel").fadeOut(300, function(){
					$("#item2-carrusel").fadeIn(300);
				});
				$("#item1-carrusel-small").fadeOut(300, function(){
					$("#item2-carrusel-small").fadeIn(300);
				});
			} else {
				$("#ocultoCarrusel").get(0).value = "0";
				$("#item2-carrusel").fadeOut(300, function(){
					$("#item1-carrusel").fadeIn(300);
				});
				$("#item2-carrusel-small").fadeOut(300, function(){
					$("#item1-carrusel-small").fadeIn(300);
				});
			}
		  setTimeout(explode, 10000);
		}
		setTimeout(explode, 10000);







		function explodedown(){
			if ($("#ocultoCarrusel").get(0).value == "0"){
				$("#ocultoCarrusel").get(0).value = "1";
				$("#item1-carrusel-down").fadeOut(300, function(){
					$("#item2-carrusel-down").fadeIn(300);
				});
				/*$("#item1-carrusel-small").fadeOut(300, function(){
					$("#item2-carrusel-small").fadeIn(300);
				});*/
			} else {
				$("#ocultoCarrusel").get(0).value = "0";
				$("#item2-carrusel-down").fadeOut(300, function(){
					$("#item1-carrusel-down").fadeIn(300);
				});
				/*$("#item2-carrusel-small").fadeOut(300, function(){
					$("#item1-carrusel-small").fadeIn(300);
				});*/
			}
		  setTimeout(explodedown, 10000);
		}
		setTimeout(explodedown, 10000);

	</script>

	<!-- Slider Revolution Main -->
	<script type="text/javascript">
	jQuery(document).ready(function() {


		







		$("#img_logo_nav").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_over_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  }
		});

		$("#img_logo_nav_text").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_over_img.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_img.png');
		  }
		});


		$("#img_loggin").on({
		 "mouseover" : function() {
		    this.src = 'images/icon_user_jj.png';
		  },
		  "mouseout" : function() {
		    this.src='images/icon_user.png';
		  }
		});




		$(window).on({
		 "scroll" : function() {
		    //$("#div-nav").height(50);
		    $("div-nav").css("height", 50);
		  }
		});

		/*$(window).on("scroll", function() {
		    //$("nav").toggleClass("shrink", $(this).scrollTop() > 50)
		    $("#div-nav").height(50);
		});*/

		


	   jQuery("#slider1").revolution({
	        sliderType:"standard",
	        startDelay:2500,
	        spinner:"spinner2",
	        sliderLayout:"auto",
	        viewPort:{
	           enable:false,
	           outof:'wait',
	           visible_area:'100%'
	        }
	        ,
	        delay:9000,
			navigation: {
				keyboardNavigation:"off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation:"off",
				onHoverStop:"off",
				arrows: {
					style:"erinyen",
					enable:true,
					hide_onmobile:true,
					hide_under:600,
					hide_onleave:true,
					hide_delay:200,
					hide_delay_mobile:1200,
					tmp:'<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div>    <div class="tp-arr-img-over"></div>	<span class="tp-arr-titleholder">{{title}}</span> </div>',
					left: {
						h_align:"left",
						v_align:"center",
						h_offset:30,
						v_offset:0
					},
					right: {
						h_align:"right",
						v_align:"center",
						h_offset:30,
						v_offset:0
					}
				}
				,
				touch:{
					touchenabled:"on",
					swipe_treshold : 75,
					swipe_min_touches : 1,
					drag_block_vertical:false,
					swipe_direction:"horizontal"
				}
				,
				bullets: {
	                enable:true,
	                hide_onmobile:true,
	                hide_under:600,
	                style:"hermes",
	                hide_onleave:true,
	                hide_delay:200,
	                hide_delay_mobile:1200,
	                direction:"horizontal",
	                h_align:"center",
	                v_align:"bottom",
	                h_offset:0,
	                v_offset:30,
	                space:5
				}
			},
			gridwidth:1240,
			gridheight:497
	    });
	});
	</script>
</body>
</html>
