<!DOCTYPE html>
<html>
<?php
$dispositivo_latitud = 2.936419;
$dispositivo_longitud = -75.288697;
?>
<head>
	<title>Find My Bicy - Rodar nunca fue tan seguro</title>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />-->
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

	<!-- Datatables CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/datatables/media/css/dataTables.bootstrap.css">



  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>


  <!-- FullCalendar Plugin CSS -->
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/magnific/magnific-popup.css">

  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/tagmanager/tagmanager.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/daterange/daterangepicker.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css">
  <link rel="stylesheet" type="text/css" href="admin/vendor/plugins/colorpicker/css/bootstrap-colorpicker.min.css">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Theme CSS -->
  	<link rel="stylesheet" type="text/css" href="admin/assets/skin/default_skin/css/theme.css">

  	<!-- Admin Forms CSS -->
  	<link rel="stylesheet" type="text/css" href="admin/assets/admin-tools/admin-forms/css/admin-forms.min.css">

  	<!-- Favicon -->
  <link rel="shortcut icon" href="admin/assets/img/favicon.ico">

	<style type="text/css">

		#btn-contacto-login:hover,
		#btn-contacto-login:focus{
			background:black;
			color: #53c5ea;
		}

		#btn-contacto-login{
			color: #FFF;
		}

		#map {
			height: 80%!important;
	        left: 0px;
	        right: 0px;
	        top: 0px;
	        bottom: 0px;


	    }
	    /* Optional: Makes the sample page fill the window. */
	    html, body {
	        height: 100%;
	        margin: 0;
	        padding: 0;
	        overflow-x:hidden;
			overflow-y:hidden;
	    }

	    .boton-file {
	    	background-image: "images/icon_file.png";

	    }

			#span_batery{
				margin-top: 10px!important;
			}

	</style>

</head>
<body>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


	<!--navbar navbar-default -->
	<nav class="navbar  navbar-static-top" style="background: black; height: 40px;">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header" style="background: black;">

	      <a class="navbar-brand" href="index.php" style="margin-top: -15px; margin-left: 20px;">
	      		<img src="images/logo_img.png" style="height: 40px; margin-top: 5px; margin-right: 20px; margin-bottom: 5px;" id="img_logo_nav" />
	      </a>
	    </div>




	    <div class="dropdown navbar-right" style="margin-right: 25px; margin-top: 10px; background: black;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="images/icon_hamburger.png" style="width: 30px;" /></a>
          <ul class="dropdown-menu" style="background: black; color: white;">
          	<li><a id="btn-contacto-login" href="perfil.php">Perfil</a></li>
          	<li><a id="btn-contacto-login" href="">Scott 740</a></li>
          	<li><a id="btn-contacto-login" href="" data-toggle="modal" data-target="#nueva_bicy">Añadir Bicy</a></li>
            <li><a id="btn-contacto-login" href="ayuda.php">Ayuda</a></li>

            <!--<li role="separator" class="divider"></li>-->
          </ul>
        </div>





				<div style="margin-top: 10px; background: black; text-align: center; color: white; height: 40px;">
						<img src="images/icon_signal_gps_100.png" style="width: 30px;" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<img src="images/icon_batery_100.png" style="width: 30px;" /> <span id="span_batery">100%</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<img src="images/icon_signal_50.png" style="width: 30px;" />

	        </div>


	  </div><!-- /.container-fluid -->
	</nav>








	<div class="modal fade" id="nueva_bicy" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
					<form role="form">
						<div class="form-group">
								<div class="input-group" style="width: 100%">
										<input class="form-control" type="text" id="txtCodigoNuevaBicy" placeholder="Código" style="width: 100%">
								</div>
						</div>
						<div class="form-group">
							<div class="input-group" style="width: 100%">
									<input type="text" class="form-control" id="txtNombreNuevaBicy" placeholder="Nombre Bicy">
								</div>
						</div>
						<div class="form-group">
							<div class="input-group ">
                            	<div class="col-md-4">

                            		<div class="fileupload fileupload-new admin-form" data-provides="fileupload">
				                        <div class="fileupload-preview thumbnail mb20">
				                        	<!--data-src="holder.js/178%x147"-->
				                          <img src="images/icon_placeholder.png">
				                        </div>
				                        <div class="row">
				                          <div class="col-xs-12 pr5">
				                            <label for="name2" class="field-icon"></label>
				                          </div>
				                          <div class="col-xs-12">
				                          	<!--button btn-system-->
				                            <span class="btn-file btn-block">
				                              <!--<span class="fileupload-new">Select</span>
				                              <span class="fileupload-exists">Change</span>-->
				                              <input type="file">
				                            </span>
				                          </div>
				                        </div>
				                    </div>

                            	</div>
                            	<div class="col-md-4">

                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
				                        <div class="fileupload-preview thumbnail mb20">
				                          <img src="images/icon_placeholder.png">
				                        </div>
				                        <div class="row">
				                          <div class="col-xs-12 pr5">
				                            <label for="name2" class="field-icon"></label>
				                          </div>
				                          <div class="col-xs-12">
				                          	<!--button btn-system-->
				                            <span class="btn-file btn-block">
				                              <!--<span class="fileupload-new">Select</span>
				                              <span class="fileupload-exists">Change</span>-->
				                              <input type="file">
				                            </span>
				                          </div>
				                        </div>
				                    </div>

                            	</div>
                            	<div class="col-md-4">

                                    <div class="fileupload fileupload-new admin-form" data-provides="fileupload">
				                        <div class="fileupload-preview thumbnail mb20">
				                          <img src="images/icon_placeholder.png">
				                        </div>
				                        <div class="row">
				                          <div class="col-xs-12 pr5">
				                            <label for="name2" class="field-icon"></label>
				                          </div>
				                          <div class="col-xs-12">
				                          	<!--button btn-system-->
				                            <span class="btn-file btn-block">
				                              <!--<span class="fileupload-new">Select</span>
				                              <span class="fileupload-exists">Change</span>-->
				                              <input type="file">
				                            </span>
				                          </div>
				                        </div>
				                    </div>

                            	</div>
							</div>
						</div>

						<button id="btn_anhadir_bicy" type="button" data-dismiss="modal" data-toggle="modal" data-target="#planes" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>AÑADIR</b></button>
						</br>

					</form>
	            </div>
	        </div>
	    </div>
	</div>









	<div class="modal fade" id="planes" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:1000px; margin-left:-500px; height:1000px; margin-top:-200px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
					<form role="form">

						<div class="row" style="text-align: center;">
							<div class="row">
								<div class="col-md-3" style="text-align: center;">
									<h2>PLAN MES</h2>
									<h4>* Caracteristica 1</h4>
									<h4>* Caracteristica 2</h4>
									<h4>* Caracteristica 3</h4>
									<h4>* Caracteristica 4</h4>
									<h4>* Caracteristica 5</h4>
									<h4>* Caracteristica 6</h4>
									<h1 style="color: black;">$20.000 COP</h1>
									<button id="btn_plan_1" type="button" data-dismiss="modal" data-toggle="modal" data-target="#credit-card" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>SELECCIONAR</b></button>
									</br>
								</div>

								<div class="col-md-3" style="text-align: center;">
									<h2>PLAN TRIMESTRE</h2>
									<h4>* Caracteristica 1</h4>
									<h4>* Caracteristica 2</h4>
									<h4>* Caracteristica 3</h4>
									<h4>* Caracteristica 4</h4>
									<h4>* Caracteristica 5</h4>
									<h4>* Caracteristica 6</h4>
									<h1 style="color: black;">$40.000 COP</h1>
									<button id="btn_plan_2" type="button" data-dismiss="modal" data-toggle="modal" data-target="#credit-card" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>SELECCIONAR</b></button>
									</br>
								</div>

								<div class="col-md-3" style="text-align: center;">
									<h2>PLAN SEMESTRE</h2>
									<h4>* Caracteristica 1</h4>
									<h4>* Caracteristica 2</h4>
									<h4>* Caracteristica 3</h4>
									<h4>* Caracteristica 4</h4>
									<h4>* Caracteristica 5</h4>
									<h4>* Caracteristica 6</h4>
									<h1 style="color: black;">$60.000 COP</h1>
									<button id="btn_plan_3" type="button" data-dismiss="modal" data-toggle="modal" data-target="#credit-card" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>SELECCIONAR</b></button>
									</br>
								</div>

								<div class="col-md-3" style="text-align: center;">
									<h2>PLAN AÑO</h2>
									<h4>* Caracteristica 1</h4>
									<h4>* Caracteristica 2</h4>
									<h4>* Caracteristica 3</h4>
									<h4>* Caracteristica 4</h4>
									<h4>* Caracteristica 5</h4>
									<h4>* Caracteristica 6</h4>
									<h1 style="color: black;">$80.000 COP</h1>
									<button id="btn_plan_3" type="button" data-dismiss="modal" data-toggle="modal" data-target="#credit-card" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>SELECCIONAR</b></button>
									</br>
								</div>
								</br>
							</div>
							</br>
							<h6></h6>
							<h6>* Al seleccionar el plan acepto los términos y condiciones.....</h6>
						</div>

					</form>
	            </div>
	        </div>
	    </div>
	</div>











	<div class="modal fade" id="credit-card" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:350px; margin-left:-175px; height:350px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
					<form role="form">

						<div class="col-md-12" style="text-align: center;">
							<input type="text" id="txt_nombre_tarjeta" name="txt_nombre_tarjeta" placeholder="Nombre en la tarjeta" class="form-control" />
							</br>
							<input type="text" id="txt_numero_tarjeta" name="txt_numero_tarjeta" placeholder="Número tarjeta" maxlength="19" class="form-control" />
							</br>
							<div class="col-md-4">
								<select id="sl_mes" name="sl_mes" class="form-control">
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select>
								</br>
							</div>
							<div class="col-md-4">
								<select id="sl_anho" name="sl_anho" class="form-control">
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
									<option value="2024">2024</option>
									<option value="2025">2025</option>
									<option value="2026">2026</option>
									<option value="2027">2027</option>
									<option value="2028">2028</option>
									<option value="2029">2029</option>
									<option value="2030">2030</option>
								</select>
								</br>
							</div>
							<div class="col-md-4">
								<input type="text" id="txt_cvc_tarjeta" name="txt_cvc_tarjeta" placeholder="CVC" class="form-control" />
								</br>
							</div>
							<button id="btn_pagar" type="button" data-dismiss="modal" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>PAGAR</b></button>
							</br>
						</div>


						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>
						</br>

					</form>
	            </div>
	        </div>
	    </div>
	</div>



















	<div id="map"></div>

	<div class="row" style="position: absolute; left: 20px; top: 30%; height: 100%; width: 100px; text-align: center;">

		<a href="#"><img src="images/icon_localizacion_mapa.png" style="height: 100px;" /></a>
		<a href="#" id="btn_modos"><img src="images/icon_parqueo_mapa.png" style="height: 100px; " /></a>
		<a href="#"><img src="images/icon_alerta_mapa.png" style="height: 100px;" /></a>

	</div>

	<div class="row" style="position: absolute; left: 150px; top: 33%; color: black; height: 100%; width: 250px; display: none;" id="div_modos">

		<a href="#" class="oculta_modo" data-toggle="modal" data-target="#mensaje_vigilancia"><img src="images/icon_movimiento.png" style="height: 50px;" /></a>&nbsp;&nbsp; <span style="background: white; opacity: 0.7; border-style: solid; border-radius: 5px 5px 5px 5px; border-color: white;">Vigilancia</span></br></br>
		<a href="#" class="oculta_modo" data-toggle="modal" data-target="#mensaje_vibracion"><img src="images/icon_parqueo_vibracion_mapa.png" style="height: 50px; " /></a>&nbsp;&nbsp; <span style="background: white; opacity: 0.7; border-style: solid; border-radius: 5px 5px 5px 5px; border-color: white;">Parqueo por vibración</span></br></br>
		<a href="#" class="oculta_modo" data-toggle="modal" data-target="#mensaje_zona"><img src="images/icon_parqueo_zona_mapa.png" style="height: 50px;" /></a>&nbsp;&nbsp; <span style="background: white; opacity: 0.7; border-style: solid; border-radius: 5px 5px 5px 5px; border-color: white;">Parqueo por zona</span></br></br>
		<a href="#" class="oculta_modo" data-toggle="modal" data-target="#mensaje_ahorro"><img src="images/icon_ahorro_energia.png" style="height: 50px;" /></a>&nbsp;&nbsp; <span style="background: white; opacity: 0.7; border-style: solid; border-radius: 5px 5px 5px 5px; border-color: white;">Ahorro de energía</span></br></br>

	</div>



	<div class="modal fade" id="mensaje_vigilancia" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
								<form role="form">
									<h3 style="width: 100%; text-align: center;">MODO VIGILANCIA</h3>
									<p>Recomendado durante el recorrido. La policia puede ver con su número de documento de identificación, la ubicación de tu Bicy. Para más información ver manual instructivo.</p>
									<input type="checkbox" name="check_vigilancia" value="0"> <span style="font-size: 12px;">No volver a mostrar</span><br><br>
									<button id="btn_mensaje_vigilancia" type="button" data-dismiss="modal" data-toggle="modal" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>ACEPTAR</b></button>
									</br>

								</form>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="mensaje_vibracion" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
								<form role="form">
									<h3 style="width: 100%; text-align: center;">MODO PARQUEO POR VIBRACION</h3>
									<p>El dispositivo notifica cuando tu Bicy ha sido movida. Para más información ver manual instructivo.</p>
									<input type="checkbox" name="check_vigilancia" value="0"> <span style="font-size: 12px;">No volver a mostrar</span><br><br>
									<button id="btn_mensaje_vigilancia" type="button" data-dismiss="modal" data-toggle="modal" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>ACEPTAR</b></button>
									</br>

								</form>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="mensaje_zona" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
								<form role="form">
									<h3 style="width: 100%; text-align: center;">MODO PARQUEO POR ZONA</h3>
									<p>El usuario fija un area por donde tu Bicy puede circular y el dispositivo notifica si sale de ella. Para más información ver manual instructivo.</p>
									<input type="checkbox" name="check_vigilancia" value="0"> <span style="font-size: 12px;">No volver a mostrar</span><br><br>
									<button id="btn_mensaje_vigilancia" type="button" data-dismiss="modal" data-toggle="modal" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>ACEPTAR</b></button>
									</br>

								</form>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="mensaje_ahorro" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:5px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <div class="modal-body">
								<form role="form">
									<h3 style="width: 100%; text-align: center;">MODO AHORRO DE ENERGIA</h3>
									<p>Este modo permite una duración de la bateria de 5 semanas, aproximadamente. Para más información ver manual instructivo.</p>
									<input type="checkbox" name="check_vigilancia" value="0"> <span style="font-size: 12px;">No volver a mostrar</span><br><br>
									<button id="btn_mensaje_vigilancia" type="button" data-dismiss="modal" data-toggle="modal" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea;"><b>ACEPTAR</b></button>
									</br>

								</form>
	            </div>
	        </div>
	    </div>
	</div>




	<div class="row" style="position: absolute; right: 50px; bottom: 70px; text-align: center;">

	</div>





	<div class="modal fade" id="modoparqueo" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="position: absolute; top:50%; left:50%; width:300px; margin-left:-150px; height:300px; margin-top:-150px; padding:0px;">
	        <div class="modal-content" style="border-radius: 10px">
	            <!--<div class="modal-header" style="background: white; border-radius: 10px">
	                <button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	                <!--<h4 class="modal-title" id="loginLabel">Ingresa</h4>- ->
	                <!--<img src="images/logo_login.png" style="width: 135px;" />- ->
	            </div>-->
	            <!--<button type="button" class="close" data-dismiss="modal" style="color: black;"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>-->
	            <div class="modal-body">
					<button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #53c5ea; color: #FFF;"><b>VIBRACIÓN</b></button>
					<button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #FFF; color: #000;">ZONA</button>
					<button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #FFF; color: #000;">MOVIMIENTO</button>
					<button type="button" class="btn btn-rw btn-primary" style="width: 100%; background: #FFF; color: #000;">OFF</button>

	            </div>
	            <!--<div class="modal-footer" style="text-align: center;">

	            </div>-->
	        </div><!-- /modal content -->
	    </div><!-- /modal dialog -->
	</div><!-- /modal holder -->











	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="js/jquery.fitvids.js"></script>



	<script type="text/javascript">
	jQuery(document).ready(function() {


		$("#txt_numero_tarjeta").attr("maxlength", 16);
		$("#txt_cvc_tarjeta").attr("maxlength", 4);

		$("#img_view_bicy").attr("src","admin/assets/img/placeholder.png");
		$("#img_view_factura").attr("src","admin/assets/img/placeholder.png");
		$("#img_view_tarjeta").attr("src","admin/assets/img/placeholder.png");




		$("#img_logo_nav").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_over_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_img.png';
		    $("#img_logo_nav_text").attr("src", 'images/logo_text.png');
		  }
		});

		$("#img_logo_nav_text").on({
		 "mouseover" : function() {
		    this.src = 'images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_over_img.png');
		  },
		  "mouseout" : function() {
		    this.src='images/logo_text.png';
		    $("#img_logo_nav").attr("src", 'images/logo_img.png');
		  }
		});



		$("#btn_modos").click(function(){
			$("#div_modos").fadeIn(1000);
		});

		$(".oculta_modo").click(function(){
			$("#div_modos").fadeOut(1000);
		});







		$(window).on({
		 "scroll" : function() {
		    //$("#div-nav").height(50);
		    $("div-nav").css("height", 50);
		  }
		});

	});
	</script>


	<script>
      var customLabel = {
        restaurant: {
          label: 'R'
        },
        bar: {
          label: 'B'
        }
      };

        function initMap() {

            var dispositivo_latitud = <?php echo $dispositivo_latitud; ?>;
            var dispositivo_longitud = <?php echo $dispositivo_longitud; ?>;
            //var dispositivo_nombre = <?php echo $dispositivo_nombre; ?>;
            var dispositivo_nombre = decodeURIComponent("<?php echo rawurlencode($dispositivo_nombre); ?>");

            var map = new google.maps.Map(document.getElementById('map'), {
              center: new google.maps.LatLng(dispositivo_latitud, dispositivo_longitud),
              zoom: 16
            });

            var infoWindow = new google.maps.InfoWindow;

            //var name = dispositivo_nombre;
            var address = "";
            var type = "";
            var point = new google.maps.LatLng(
                parseFloat(dispositivo_latitud),
                parseFloat(dispositivo_longitud));

            var infowincontent = document.createElement('div');
            var strong = document.createElement('strong');
            strong.textContent = dispositivo_nombre
            infowincontent.appendChild(strong);
            infowincontent.appendChild(document.createElement('br'));

            var text = document.createElement('text');
            text.textContent = address
            infowincontent.appendChild(text);

            var icon = customLabel[type] || {};
            var marker = new google.maps.Marker({
              map: map,
              position: point,
              label: icon.label
            });
            marker.addListener('click', function() {
              infoWindow.setContent(infowincontent);
              infoWindow.open(map, marker);
            });
        }

        initMap();


    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSASLjBcHC5YEIPjqSo8Rs2KZoF3REG3c&callback=initMap">
    </script>



    <!-- jQuery -->
  <script src="admin/vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="admin/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- HighCharts Plugin -->
  <script src="admin/vendor/plugins/highcharts/highcharts.js"></script>

  <!-- JvectorMap Plugin + US Map (more maps in plugin/assets folder) -->
  <script src="admin/vendor/plugins/jvectormap/jquery.jvectormap.min.js"></script>
  <script src="admin/vendor/plugins/jvectormap/assets/jquery-jvectormap-us-lcc-en.js"></script>

  <!-- FullCalendar Plugin + moment Dependency -->
  <script src="admin/vendor/plugins/fullcalendar/lib/moment.min.js"></script>
  <script src="admin/vendor/plugins/fullcalendar/fullcalendar.min.js"></script>

  <!-- Magnific Popup Plugin -->
  <script src="admin/vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <!--<script src="admin/assets/js/utility/utility.js"></script>
  <script src="admin/assets/js/demo/demo.js"></script>
  <script src="admin/assets/js/main.js"></script>-->

  <script src="admin/assets/js/demo/demo.js"></script>
  <script src="admin/assets/js/main.js"></script>

  <!-- Select2 Plugin Plugin -->
  <script src="admin/vendor/plugins/select2/select2.min.js"></script>






  <!-- Time/Date Plugin Dependencies -->
  <script src="admin/vendor/plugins/globalize/globalize.min.js"></script>
  <script src="admin/vendor/plugins/moment/moment.min.js"></script>

  <!-- Bootstrap Maxlength plugin -->
  <script src="admin/vendor/plugins/maxlength/bootstrap-maxlength.min.js"></script>

  <!-- DateTime Plugin -->
  <script src="admin/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>

  <!-- MaskedInput Plugin -->
  <script src="admin/vendor/plugins/jquerymask/jquery.maskedinput.min.js"></script>




  <!-- FileUpload JS -->
  <script src="admin/vendor/plugins/fileupload/fileupload.js"></script>
  <script src="admin/vendor/plugins/holder/holder.min.js"></script>

  <!-- Tagmanager JS -->
  <script src="admin/vendor/plugins/tagsinput/tagsinput.min.js"></script>








  <!-- Datatables -->
  <script src="admin/vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

  <!-- Datatables Tabletools addon -->
  <script src="admin/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

  <!-- Datatables ColReorder addon -->
  <script src="admin/vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>

  <!-- Datatables Bootstrap Modifications  -->
  <script src="admin/vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>


  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core
    //Core.init();

    // Init Demo JS
    Demo.init();

    // select list dropdowns - placeholder like creation
    var selectList = $('.admin-form select');
    selectList.each(function(i, e) {
      $(e).on('change', function() {
        if ($(e).val() == "0") $(e).addClass("empty");
        else $(e).removeClass("empty")
      });
    });
    selectList.each(function(i, e) {
      $(e).change();
    });

    // Init TagsInput plugin
    $("input#tagsinput").tagsinput({
      tagClass: function(item) {
        return 'label bg-primary light';
      }
    });

  });
  </script>




  <style type="text/css">

        .file-upload {
            position: relative;
            overflow: hidden;
            margin: 10px;
        }

        .file-upload input.file-input {
          position: absolute;
          top: 0;
          right: 0;
          margin: 0;
          padding: 0;
          font-size: 20px;
          cursor: pointer;
          opacity: 0;
          filter: alpha(opacity=0);
        }





    </style>

</body>
</html>
